/* Pulls together numbers from the various files to populate tables and figures */
cd "C:\Users\matte\Documents\cms_star_ratings\cms-star-ratings-methodology-final-code-and-data"

*
/*
Hospitals

All				Count Rank (min-max)	5* 4-2* 1*
Seven
Six
Five
Three-Four
*/

* Load in useful data
use data/approaches_combined.dta, clear

keep provider_id std1_grp1_wt1* grp1_cat grp1_missing
expand 2, gen(exp)

label list grp_cat

replace grp1_cat = -1 if exp
replace grp1_cat = 2 if grp1_cat == 3
label define grp_cat 	-1 "All hospitals" /// 
						0 "All seven domains reported" ///
						1 "Six domains reported" ///
						2 "Three-five domains reported" /// 
						, modify


keep if !missing(std1_grp1_wt1_score)
gen five  = std1_grp1_wt1_star == 5
gen four  = std1_grp1_wt1_star == 4
gen three = std1_grp1_wt1_star == 3
gen two   = std1_grp1_wt1_star == 2
gen one   = std1_grp1_wt1_star == 1

gen any = 1

collapse	(sum) any five four three two one ///
			(median) p50 = std1_grp1_wt1_rank ///
			(p25) p25 = std1_grp1_wt1_rank (p75) p75 = std1_grp1_wt1_rank ///
			(min) min = std1_grp1_wt1_rank (max) max = std1_grp1_wt1_rank ///
			, by( grp1_cat )
			
order grp1_cat any five four three two one p50 p25 p75 min max
list, clean noobs

* baseline performance
use data/approaches_combined.dta, clear

keep provider_id std1_grp1_wt1* grp1_cat grp1_missing
rename std1_grp1_wt1_score baseline_score
rename std1_grp1_wt1_rank baseline_rank
tab *_stars
table *_stars, c(max baseline_rank)
rename *_stars baseline_stars
 

keep provider_id baseline_score baseline_rank baseline_stars grp1_cat grp1_missing
compress
save paper_data/paper_baselines, replace

* rating in each stars
use simdata/cms_weights_std1_grp1_sims.dta, clear
merge m:1 provider_id using paper_data/paper_baselines, assert(2 3)
drop if _merge == 2 // not enough data to assign star rating, basically
count if sim == 1 // 3726 good
drop _merge

replace wtd_summary_rank = 3726-wtd_summary_rank+1

save paper_data/rankdata, replace

use paper_data/rankdata, clear

forval i = 2/5 {
	count if baseline_stars >= `i' & sim == 1
	local stars`i' = r(N)
}

gen stars5 = wtd_summary_rank <= `stars5'
gen stars4 = wtd_summary_rank <= `stars4' & !stars5
gen stars3 = wtd_summary_rank <= `stars3' & !stars5 & !stars4
gen stars2 = wtd_summary_rank <= `stars2' & !stars5 & !stars4 & !stars3
gen stars1 = !stars2 & !stars3 & !stars4 & !stars5


gen grp2_cat =grp1_cat
replace grp2_cat = 2 if grp1_cat == 3
label define grp_cat 	-1 "All hospitals" /// 
						0 "All seven domains reported" ///
						1 "Six domains reported" ///
						2 "Three-five domains reported" /// 
						, modify
label values grp2_cat grp_cat

* overall predictive value
summ stars? if baseline_stars == 5
summ stars? if baseline_stars == 4
summ stars? if baseline_stars == 3
summ stars? if baseline_stars == 2
summ stars? if baseline_stars == 1

* predictive value, all seven domains
summ stars? if baseline_stars == 5 & grp2_cat == 0
summ stars? if baseline_stars == 4 & grp2_cat == 0
summ stars? if baseline_stars == 3 & grp2_cat == 0
summ stars? if baseline_stars == 2 & grp2_cat == 0
summ stars? if baseline_stars == 1 & grp2_cat == 0

* predictive value, six domains
summ stars? if baseline_stars == 5 & grp2_cat == 1
summ stars? if baseline_stars == 4 & grp2_cat == 1
summ stars? if baseline_stars == 3 & grp2_cat == 1
summ stars? if baseline_stars == 2 & grp2_cat == 1
summ stars? if baseline_stars == 1 & grp2_cat == 1

* predictive value, three-five domains
summ stars? if baseline_stars == 5 & grp2_cat == 2
summ stars? if baseline_stars == 4 & grp2_cat == 2
summ stars? if baseline_stars == 3 & grp2_cat == 2
summ stars? if baseline_stars == 2 & grp2_cat == 2
summ stars? if baseline_stars == 1 & grp2_cat == 2

* scatter plot
preserve
keep if inlist(sim, 514, 2857, 2969, 3588)
list sim wt_d* in 1
set scheme s1color

#delimit ;
label define sim	514 "A. Simulation 514"
					2857 "B. Simulation 2857"
					2969 "C. Simulation 2969"
					3588 "D. Simulation 3588"
	;
#delimit cr
label values sim sim

#delimit ;
twoway	(
	scatter wtd_summary_rank baseline_rank if baseline_stars == 5 & grp2_cat == 2
	, 	msymb(oh) mcol(gs4%50)
	)
	(
	scatter wtd_summary_rank baseline_rank if baseline_stars == 5 & grp2_cat == 0
	, 	msymb(o ) mcol(red%40) 
	)
	(
	scatter wtd_summary_rank baseline_rank if baseline_stars == 5 & grp2_cat == 1
	, 	msymb(d ) mcol(blue%40)
	)
	
	,	by(sim, note(""))
		legend(
			order(
				2 "All seven domains reported" 
				3 "Six domains reported"
				1 "3-5 domains reported"
			)
			symxs(*5)
			region(lstyle(none))
			cols(3)
			size(small)
		)
		subtitle(
			,	pos(11)
				fcolor(gs0)
				color(gs16)
		)
		ylabel(
			 80    "5 star"
			705.5  "4 star"
			1843   "3 star"
			2964.5 "2 star"
			3700   "1 star"
			,	angle(h)
				tl(0)
				nogrid
		)
		ytick(
			225
			1186
			2500
			3429
			,	tl(0)
				grid
		)
		ysc(noline r(1 3726))
		xlabel(1 75 150 225)
		xsc(noline)
		xtitle("Rank under 2020 CMS approach")
		ytitle("Rank and star rating under alternative approach")
		
;
#delimit cr
graph export figure1_paper.pdf, xsize(8) ysize(6) replace
restore


* table of weights
use simdata/cms_weights_std1_grp1_sims.dta, clear

keep if inlist(sim, 514, 2857, 2969, 3588)

forval d = 1/7 {
	foreach sim in 514 2857 2969 3588 {
		summ wt_d`d' if sim == `sim'
		replace wt_d`d' = r(mean) if sim == `sim'
	}
	
}

sort sim provider_id
by sim: keep if _n == 1

egen wt_tot = rowtotal(wt_d*)

forval d = 1/7 {
	replace wt_d`d' = wt_d`d'/wt_tot
}


format wt_d* %03.2f
list sim wt_d* 
