*Note: it takes less than 5 minutes to run this program;
***********************************************;
* Create Star analysis data file 			  *;
*											  *;
* SAS 9.4 WIN			   					  *;
*											  *;
* YALE CORE 	  		                      *;
*											  *;
* email: cmsstarratings@lantanagroup.com      *; 
***********************************************;
/*This file is used to derive analysis data 'Std_data_2020Jan_analysis', which will be used in programs 1 and 2*/

%LET PATH1=H:\Star Rating\2020\2020 Jan SAS pack\input data ; /* For 2020 Jan released data, MUST BE CHANGED */
%LET PATH2=H:\Star Rating\2020\2020 Jan SAS pack\derived data ; /* For derived data sets, MUST BE CHANGED */
%LET PATH3=H:\Star Rating\2020\2020 Jan SAS pack ; /* For SAS macros , MUST BE CHANGED  */

LIBNAME HC "&PATH1";
LIBNAME R "&PATH2";

%INCLUDE "&path3.\Star_Macros.sas";/*calling macros*/

%LET year = 2020;
%LET quarter = Jan;

%LET MEASURE_MEAN=R.measure_average_stddev_&year.&quarter; /*mean and standard deviation of the original measure scores*/
%LET MEASURE_ANALYSIS=R.Std_data_&year.&quarter._analysis; /* derived final analysis file */
%LET RESULTS=R.Star_&year.&quarter; /* derive hospital-level Star file */
%LET NATIONAL_MEAN=R.national_average_&year.&quarter; /* derived national average group and summary scores results */


proc printto new log="&path2.\program0.log" print="&path2.\program0.lst";run;*output LOG and LST files;

/* 57 measures on Hospital Campare in 2020 Jan, after excluding re-tired measures, no-directional measures, 
	structural measures, and voluntary measures. Please note that IMM_3 and OP_27 are combined */
%LET measure_all = COMP_HIP_KNEE ED_1B ED_2B EDAC_30_AMI EDAC_30_HF EDAC_30_PN HAI_1 HAI_2 
				   HAI_3 HAI_4 HAI_5 HAI_6 H_CLEAN_HSP_LINEAR H_COMP_1_LINEAR H_COMP_2_LINEAR 
				   H_COMP_3_LINEAR H_COMP_5_LINEAR H_COMP_6_LINEAR 
				   H_COMP_7_LINEAR H_HSP_RATING_LINEAR H_QUIET_HSP_LINEAR H_RECMND_LINEAR 
				   IMM_2 IMM_3 MORT_30_AMI MORT_30_CABG MORT_30_COPD MORT_30_HF 
				   MORT_30_PN MORT_30_STK OP_2 OP_3B OP_5 OP_8 OP_10 OP_11 
				   OP_13 OP_14 OP_18B OP_22 OP_23 OP_29 OP_30 OP_32 OP_33 /*OP_35 OP_36*/
				   PC_01 PSI_4_SURG_COMP PSI_90_SAFETY READM_30_CABG READM_30_COPD 
				   READM_30_HIP_KNEE READM_30_HOSP_WIDE  
                   SEP_1 VTE_6;*For 2020 Jan, READM_30_STK, OP_20, OP_21, OP_4 and OP_27 (IMM_3 stays) are removed;

/* denominator for measuers in patient experience measure group*/
%LET den_PtExp = H_NUMB_COMP H_RESP_RATE_P; 


************************************************************************
* APPLY THE FOLLOWING EXCLUSION CRITERIA:                              *
* MRWAURE VOLUME <=100                                                 *
************************************************************************;

/* PRE-PROCESS 2020 Jan HOSPITAL COMPARE DATA INTO ONE FILE, WHICH INCLUDES ALL NEEDED DENOMINATOR VARIABLES */

/*IMM-3/OP-27: Healthcare Personnel Influenza Vaccination*/
/* COMBINE IMM-3 and OP-27 */

DATA All_data_&year.&quarter ;
set HC.All_data_&year.&quarter.;

*let HAI_x_Den equal to HAI_x_DEN_VOL (cumulative exposure);
HAI_1_DEN=HAI_1_DEN_VOL;
HAI_2_DEN=HAI_2_DEN_VOL;
HAI_3_DEN=HAI_3_DEN_VOL;
HAI_4_DEN=HAI_4_DEN_VOL;
HAI_5_DEN=HAI_5_DEN_VOL;
HAI_6_DEN=HAI_6_DEN_VOL;
run;


*************************************************************
**set as missing for any measures that dont have a denominator*
*************************************************************;
PROC TABULATE DATA=All_data_&year.&quarter out=measure_volume0; 
var &measure_all;

table n*(&measure_all);
RUN;

PROC TRANSPOSE data=Measure_volume0 out=measure_volume_t0;
RUN;

DATA include_measure0  (drop=_NAME_ _LABEL_ rename = (COL1=freq)); 
SET measure_volume_t0;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_std = 'std_'||trim(measure_in_name);

measure_in_den = trim(measure_in_name)||'_DEN'; 
RUN;

PROC SQL;
select measure_in_name into: measure_in separated by '' notrim
from include_measure0;
QUIT;
%put &measure_in;

PROC SQL;
select measure_in_den into: measure_in_den separated by '' notrim
from include_measure0;
QUIT;
%put &measure_in_den;

/* &measure_cnt: number of included measure */
PROC SQL;
select count(measure_in_name)
into: measure_cnt
from include_measure0;
QUIT;
%put &measure_cnt;

DATA All_data_&year.&quarter (drop=i);
SET All_data_&year.&quarter;
	ARRAY M(&measure_cnt) &measure_in;
    ARRAY W(&measure_cnt) &measure_in_den;


*calculate den for PtExp group;
PtExp_DEN= H_NUMB_COMP*H_RESP_RATE_P/100;
H_CLEAN_HSP_LINEAR_DEN= PtExp_DEN;
H_COMP_1_LINEAR_DEN= PtExp_DEN;
H_COMP_2_LINEAR_DEN= PtExp_DEN;
H_COMP_3_LINEAR_DEN= PtExp_DEN;
H_COMP_5_LINEAR_DEN= PtExp_DEN;
H_COMP_6_LINEAR_DEN= PtExp_DEN;
H_COMP_7_LINEAR_DEN= PtExp_DEN;
H_HSP_RATING_LINEAR_DEN= PtExp_DEN;
H_QUIET_HSP_LINEAR_DEN= PtExp_DEN;
H_RECMND_LINEAR_DEN= PtExp_DEN;


    DO I =1 TO &measure_cnt;
    If W[I]=. Then M[I]=.;* not include any measures that dont have a denominator;
    END;
run;


/*COUNT # HOSPITALS PER MEASURE FOR ALL 60 MEASURES*/
PROC TABULATE DATA=All_data_&year.&quarter out=measure_volume; 
var &measure_all;

table n*(&measure_all);
RUN;

PROC TRANSPOSE data=Measure_volume out=measure_volume_t;
RUN;

/* IDENTIFY MEASURES WITH VOLUMN <=100 */
DATA less100_measure  (drop=_NAME_ _LABEL_ rename = (COL1=freq)); 
SET measure_volume_t (where = (col1<=100));

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_name = tranwrd(_NAME_, '_N', '');

measure_den = trim(measure_name)||'_DEN';
RUN;
DATA R.less100_measure;SET less100_measure;run;

/* CREATE a measure list for count<=100 */
PROC SQL;
select measure_Name
into: measure_exclude separated by '' notrim
from Less100_measure;
QUIT;

PROC SQL;
select measure_den
into: measure_exclude_den separated by '' notrim
from Less100_measure;
quit;

/* REMOVE MEASURES WHICH HAVE HOSPITAL COUNTS <=100*/
DATA initial_data_&year.&quarter;
SET All_data_&year.&quarter;

/* measure volume <=100*/
drop &measure_exclude &measure_exclude_den;
RUN;

************************************************************************
* CREATE NEEDED MEASURE NAME LIST                                      *
************************************************************************;
/* CREATE THE FINAL LISTS OF NAME OF INCLUDED MEASURES: &measure_in, &measure_in_std  */
DATA include_measure  (drop=_NAME_ _LABEL_ rename = (COL1=freq)); 
SET measure_volume_t (where = (col1>100));

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_std = 'std_'||trim(measure_in_name);

measure_in_den = trim(measure_in_name)||'_DEN'; 
RUN;

PROC SQL;
select measure_in_name into: measure_in separated by '' notrim
from include_measure;
QUIT;
%put &measure_in;

PROC SQL;
select measure_in_std into: measure_in_std separated by '' notrim
from include_measure;
QUIT;

PROC SQL;
select count(measure_in_name)
into: measure_cnt
from include_measure;
QUIT;
%put &measure_cnt;


/* CREATE THE FINAL LISTS OF EACH MEASURE GROUP AND REMOVE THOSE MEASURES WITH <= 100 HOSPITALS */

/* FINAL measuer list for outcome-mortality measure group: &measure_OM and &den_OM - MUST CHANGE WHEN NEW MEASURE ADDED/REMOVED*/
DATA outcomes_mortality (where = (col1>100)); 
SET measure_volume_t;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_den = trim(measure_in_name)||'_DEN';
measure_in_std = 'std_'||trim(measure_in_name);

/*** Here are ALL 7 Mortality measures in 2020 Jan  ***/
if measure_in_name in /*- MUST CHECK NEXT LINE AND UPDATE FOR EACH QUARTER DATA IF NEW MEASURE ADDED/REMOVED IN THIS QUARTER*/   
('MORT_30_AMI', 'MORT_30_CABG', 'MORT_30_COPD', 'MORT_30_HF', 'MORT_30_PN', 'MORT_30_STK', 'PSI_4_SURG_COMP');
RUN;

PROC SQL;
select measure_in_std
into: measure_OM separated by '' notrim
from outcomes_mortality;
QUIT;
%put &measure_OM;

PROC SQL;
select measure_in_den
into: den_OM separated by '' notrim
from outcomes_mortality;
QUIT;
%put &den_OM;

/* FINAL measuer list for outcome-safety measure group: &measure_OS and &den_OS */
DATA outcomes_safety  (where = (col1>100)); 
SET measure_volume_t;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_den = trim(measure_in_name)||'_DEN';
measure_in_std = 'std_'||trim(measure_in_name);

/*** Here are ALL 8 SAFETY measures in 2020 Jan  ***/
if measure_in_name in /*- MUST CHECK NEXT LINE AND UPDATE FOR EACH QUARTER DATA IF NEW MEASURE ADDED/REMOVED IN THIS QUARTER*/ 
('COMP_HIP_KNEE',  'HAI_1', 'HAI_2', 'HAI_3', 'HAI_4', 'HAI_5', 'HAI_6', 'PSI_90_SAFETY');
RUN;

PROC SQL;
select measure_in_std  into: measure_OS separated by '' notrim
from outcomes_safety;
QUIT;
%put &measure_OS;
PROC SQL;
select measure_in_den  into: den_OS separated by '' notrim
from outcomes_safety;
QUIT;

/* FINAL measuer list for outcome-readmission measure group: &measure_OR and &den_OR */
DATA outcomes_readmission (where = (col1>100)); 
SET measure_volume_t;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_den = trim(measure_in_name)||'_DEN';
measure_in_std = 'std_'||trim(measure_in_name);

/*** Here are ALL 10 READMISSION measures in 2020 Jan  ***/
if measure_in_name in /*- MUST CHECK NEXT LINE AND UPDATE FOR EACH QUARTER DATA IF NEW MEASURE ADDED/REMOVED IN THIS QUARTER*/ 
('EDAC_30_AMI', 'EDAC_30_HF', 'EDAC_30_PN', 'OP_32','READM_30_CABG', 'READM_30_COPD', 'READM_30_HIP_KNEE', 
'READM_30_HOSP_WIDE' /*OP_35, OP_36*/);
RUN;

PROC SQL;
select measure_in_std
into: measure_OR separated by '' notrim
from outcomes_readmission;
QUIT;
%put &measure_OR;

PROC SQL;
select measure_in_den
into: den_OR separated by '' notrim
from outcomes_readmission;
QUIT;
%put &den_OR;

/* FINAL measuer list for patient experience measure group: &measure_PtExp and &den_PtExp */
DATA PtExp (where = (col1>100)); 
SET measure_volume_t;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_den = trim(measure_in_name)||'_DEN';
measure_in_std = 'std_'||trim(measure_in_name);

/*** Here are ALL 10 PATIENT EXPERIENCE measures in 2020 Jan ***/
if measure_in_name in /*- MUST CHECK NEXT LINE AND UPDATE FOR EACH QUARTER DATA IF NEW MEASURE ADDED/REMOVED IN THIS QUARTER*/ 
('H_COMP_1_LINEAR', 'H_COMP_2_LINEAR', 'H_COMP_3_LINEAR', 'H_COMP_5_LINEAR', 'H_COMP_6_LINEAR',
'H_COMP_7_LINEAR', 'H_CLEAN_HSP_LINEAR', 'H_QUIET_HSP_LINEAR', 'H_HSP_RATING_LINEAR', 'H_RECMND_LINEAR');
RUN;

PROC SQL;
select measure_in_std
into: measure_PtExp separated by '' notrim
from PtExp;
QUIT;
%put &measure_PtExp;

/* FINAL measuer list for efficiency measure group: &measure_Ef and &den_Ef */

DATA Efficiency (where = (col1>100)); 
SET measure_volume_t;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_den = trim(measure_in_name)||'_DEN';
measure_in_std = 'std_'||trim(measure_in_name);

/*** Here are ALL 5 EFFICIENCY measures in 2020 Jan ***/
if measure_in_name in /*- MUST CHECK NEXT LINE AND UPDATE FOR EACH QUARTER DATA IF NEW MEASURE ADDED/REMOVED IN THIS QUARTER*/ 
('OP_8', 'OP_10', 'OP_11', 'OP_13', 'OP_14');
RUN;

PROC SQL;
select measure_in_std
into: measure_Ef separated by '' notrim
from Efficiency;
QUIT;
%put &measure_Ef;

PROC SQL;
select measure_in_den
into: den_Ef separated by '' notrim
from Efficiency;
QUIT;
%put &den_Ef;

/* FINAL measuer list for Process Timeliness measure group: &measure_ProcessT and &den_ProcessT*/
DATA ProcessT (where = (col1>100)); 
SET measure_volume_t;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_den = trim(measure_in_name)||'_DEN';
measure_in_std = 'std_'||trim(measure_in_name);

/*** Here are ALL 6 PROCESS TIMELINESS measures in 2020 Jan ***/
if measure_in_name in /*- MUST CHECK NEXT LINE AND UPDATE FOR EACH QUARTER DATA IF NEW MEASURE ADDED/REMOVED IN THIS QUARTER*/ 
('ED_1B', 'ED_2B', 'OP_2', 'OP_3B', 'OP_5', 'OP_18B');
RUN;

PROC SQL;
select measure_in_std
into: measure_ProcessT separated by '' notrim
from ProcessT;
QUIT;
%put &measure_ProcessT;

PROC SQL;
select measure_in_den
into: den_ProcessT separated by '' notrim
from ProcessT;
QUIT;
%put &den_ProcessT;

/* FINAL measuer list for Process Effectiveness measure group: &measure_ProcessE and &den_ProcessE*/

DATA ProcessE (where = (col1>100)); 
SET measure_volume_t;

if _name_ ^= '_PAGE_'  and _name_^='_TABLE_';
measure_in_name = tranwrd(_NAME_, '_N', '');

measure_in_den = trim(measure_in_name)||'_DEN';
measure_in_std = 'std_'||trim(measure_in_name);

/*** Here are ALL 10 Process Effectiveness measures in 2020 Jan ***/
if measure_in_name in /*- MUST CHECK NEXT LINE AND UPDATE FOR EACH QUARTER DATA IF NEW MEASURE ADDED/REMOVED IN THIS QUARTER*/ 
('IMM_2', 'IMM_3', 'OP_22', 'OP_23',  'OP_29', 'OP_30','OP_33', 'PC_01', 'SEP_1', 'VTE_6');
RUN;

PROC SQL;
select measure_in_std
into: measure_ProcessE separated by '' notrim
from ProcessE;
QUIT;
%put &measure_ProcessE;

PROC SQL;
select measure_in_den
into: den_ProcessE separated by '' notrim
from ProcessE;
QUIT;
%put &den_ProcessE;

******************************************************************
* REMOVE HOSPITALS WHICH DO NOT HAVE ANY FINAL INCLUDED MEASURES *
******************************************************************;

%keep_hos(Initial_data_&year.&quarter.,&measure_in,&measure_cnt);


**************************************************************
**Add an output file for mean and standard deviation of measure scores
**************************************************************;
proc means data=Initial_data_&year.&quarter;
var &measure_in;
output out=&MEASURE_MEAN;
run;




******************************
* Standardize Measure Scores *
******************************;
PROC STANDARD data=Initial_data_&year.&quarter mean=0 std=1 out=R.Std_data_&year.&quarter;
var	&measure_in;
run;

**********************
* RE-DIRECT MEASURES *
**********************;
/* OUTPUTS OF 'std_data_2020Jan' ARE GENERATED*/
DATA R.Std_data_&year.&quarter;
SET R.Std_data_&year.&quarter;

/* flip meaures which have negative direction, so all measuers are in the same direction 
   and a higher score means better 
   -- MUST CHECK AND UPDATE LINE 465-510 IF MEASURES ARE REMOVED, OR MEASURES ARE ADDED AND NEED TO BE FLIPPED IN NEW QUARTERS*/

HAI_1=-HAI_1;
HAI_2=-HAI_2;
HAI_3=-HAI_3;
HAI_4=-HAI_4;
HAI_5=-HAI_5;
HAI_6=-HAI_6;

EDAC_30_AMI = -EDAC_30_AMI;
EDAC_30_HF  = -EDAC_30_HF;
EDAC_30_PN = -EDAC_30_PN;
OP_32 = -OP_32;
READM_30_CABG = -READM_30_CABG;
READM_30_COPD = -READM_30_COPD;
READM_30_HIP_KNEE = -READM_30_HIP_KNEE;
READM_30_HOSP_WIDE = -READM_30_HOSP_WIDE;
*OP_35=-OP_35;
*OP_36=-OP_36;

PSI_4_SURG_COMP = -PSI_4_SURG_COMP;
PSI_90_SAFETY = -PSI_90_SAFETY;

COMP_HIP_KNEE = -COMP_HIP_KNEE;
MORT_30_AMI = -MORT_30_AMI;
MORT_30_CABG = -MORT_30_CABG;
MORT_30_COPD = -MORT_30_COPD;
MORT_30_HF = -MORT_30_HF;
MORT_30_PN = -MORT_30_PN;
MORT_30_STK = -MORT_30_STK;

ED_1B = -ED_1B;
ED_2B = -ED_2B;

OP_8 = -OP_8;
OP_10 = -OP_10;
OP_11 = -OP_11;
OP_13 = -OP_13;
OP_14 = -OP_14;

OP_3B = -OP_3B;
OP_5 = -OP_5;
OP_18B = -OP_18B;
OP_22 = -OP_22;

PC_01 = - PC_01;

VTE_6 = - VTE_6;
RUN;

*************************************************************
** Winsorization standardized measure scores at Z= -3 & Z=3 *
*************************************************************;
/* OUTPUTS OF 'std_data_2020_Jan_analysis' ARE GENERATED*/
DATA &MEASURE_ANALYSIS (drop=i &measure_in);
SET R.Std_data_&year.&quarter;

    ARRAY M(&measure_cnt) &measure_in;

    ARRAY Y(&measure_cnt) &measure_in_std; 

    DO I =1 TO &measure_cnt;
       Y[I] = m[I];  

       if Y[I] < -3 and Y[I] ^=. then Y[I] =-3;
       else if Y[I] > 3 then Y[I]=3;
    END;

run;


proc printto;run;*end of LOG/LST file output;
/*Continue to "1 - First stage_LVM_2020Jan" SAS progam*/

