proc format;
	value pred
		0 = 'Zero'
		. = 'Missing'
		other = 'Not Zero or Missing'
		;
run;

*****************************************************************************
* macro for removing hospitals which do not have any final included measures*
*****************************************************************************;

%macro keep_hos(indsn, varlist, nmeasure);

		data &indsn(where = (Total_m_cnt>=1) drop=c1-c&nmeasure. k); 
		set &indsn;

		array M(&nmeasure.) &varlist.;
		array C (1:&nmeasure.) C1-C&nmeasure.;

        DO k =1 TO &nmeasure.;
		if m[k] ^=. then C[k]=1;
   		else C[k]=0;

		Total_m_cnt=sum(of c1-c&nmeasure.);
		END;
		run;
%mend;

**************************************************
* macro for subset of data for each measure group*
**************************************************;

%macro sub_data(indsn, gp, varlist, varlist_den, nmeasure);
		data hos_&gp. (keep=provider_id &varlist. &varlist_den. c1-c&nmeasure. total_cnt id); 
		set &indsn.;

		array M(&nmeasure.) &varlist.;
		array C (1:&nmeasure.) C1-C&nmeasure.;

        DO k =1 TO &nmeasure.;
		if m[k] ^=. then C[k]=1;
   		else C[k]=0;

		total_cnt=sum(of c1-c&nmeasure.);
		id=_N_;
		END;

		run;
%mend;

******************************************************************************************
* macro for calculating the weights by denominator for accounting for sampling variation *
*         for each measure group, except patient experience group                        *
******************************************************************************************;

%macro denominator(gp, denlist, measure, nmeasure);

		%Do k = 1 %To &nmeasure.;
		%global N&k.;
		proc sql; select sum(C&k.) into:N&k. from hos_&gp.; quit;
		%End;

		proc sql;
		create table den_&gp. as
		select *,

		%Do i=1 %To (&nmeasure.-1);
		%global N&i.;

		%let VAR1 = %SCAN(&denlist, %EVAL(&i));

	    &var1./sum(&var1.)*&&N&i. as n_w_&i.,
		%End;
		
	    %let VAR2 = %SCAN(&denlist, %EVAL(&nmeasure.));

		&var2./sum(&var2.)*&&N&nmeasure. as n_w_&nmeasure.

		from hos_&gp. (keep=provider_id &denlist &measure id total_cnt);
		quit;
	
%mend;


******************************************************************************************
* macro for calculating the weights by denominator for accounting for sampling variation *
*       for patient experience group only												 *
******************************************************************************************;

%macro denominator_PtExp(gp, denlist, measure, nmeasure);

		%Do k = 1 %To &nmeasure.;
		%global N&k.;
		proc sql; select sum(C&k.) into:N&k. from hos_&gp.; quit;
		%End;

		proc sql;
		create table Den_PtExp as
		select *,
	   		    H_NUMB_COMP*H_RESP_RATE_P/100 as n,
				%Do i=1 %To (&nmeasure.-1);
	   			(calculated n)/sum(calculated n)*&&N&i. as n_w_&i.,
				%End;
	   			(calculated n)/sum(calculated n)*&&N&nmeasure. as n_w_&nmeasure.
		from hos_&gp. (keep=provider_id &denlist &measure id total_cnt);
		quit;
	
%mend;

**********************************************************
* macro for calcuating group score for each measure group*
**********************************************************;

*Step1: get initial values using non-adaptive quadrature;
%macro LVM_ini(gp, varlist, nmeasure, Out_Est);
proc nlmixed data=den_&gp. tech=dbldog qpoints=30 noad;
parms mu1-mu&nmeasure.=0 gamma1-gamma&nmeasure.=0.1;

array y{*} &varlist;
array mu{*} mu1-mu&nmeasure;
array gamma{*} gamma1-gamma&nmeasure;
array log_den{*} log_den1-log_den&nmeasure;
array w{*} n_w_1-n_w_&nmeasure;
array err{*} err1-err&nmeasure;
	        
logmu = 0; 

do i=1 to &nmeasure; 

	   if y{i} = . then log_den{i}= 0;
	       else   log_den{i} = w{i} * log (pdf('normal', y{i}, mu{i} + gamma{i}*alpha, err{i})); 

       logmu = log_den{i} + logmu;
end;

model id ~ general(logmu); 

random alpha ~ normal(0, 1) subject= provider_id;

ods output ParameterEstimates=&Out_Est; 
run;
%mend LVM_ini;

*Step2: uisng initial values from Step1 and adaptive quadrature to make estimations more accurate;
%macro LVM1(gp, varlist, nmeasure, in, Out_Est, Out_Pred);
proc nlmixed data=den_&gp. tech=dbldog qpoints=15 xtol=1e-4;
ods output convergencestatus=QAcstatus&gp;
parms/data =&in;

array y{*} &varlist;
array mu{*} mu1-mu&nmeasure;
array gamma{*} gamma1-gamma&nmeasure;
array log_den{*} log_den1-log_den&nmeasure;
array w{*} n_w_1-n_w_&nmeasure;
array err{*} err1-err&nmeasure;
	        
logmu = 0; 

do i=1 to &nmeasure; 

	   if y{i} = . then log_den{i}= 0;
	       else   log_den{i} = w{i} * log (pdf('normal', y{i}, mu{i} + gamma{i}*alpha, err{i}));

       logmu = log_den{i} + logmu;
end;

model id ~ general(logmu); 

random alpha ~ normal(0, 1) subject= provider_id;

ods output ParameterEstimates=&Out_Est;
predict alpha out=&Out_Pred; 
run;

/* hospitals with no measures in each domain should have prediction '.'
   predicion 0 is actually due to no measures in the measure group 
   - change it to '.' for the redistributed weights later*/

data &Out_Pred;
set &Out_Pred;
if total_cnt=0 then pred=.;
if total_cnt=0 then lower=.;
if total_cnt=0 then upper=.;
if total_cnt=0 then StdErrPred=.;
run;

	proc print data=QAcstatus&gp noobs;
        title "QA: (&GP.) convergence status";
    run;
	proc freq data=&out_pred;
        title "QA: (&GP.) distribution of PRED";
        tables total_cnt * pred / list missing out=QApreddist_&gp;
        format total_cnt pred pred.;
    run;
    proc print data=&out_est noobs;
        title "QA: (&GP.) Coefficients";
    run;
%mend LVM1;
%macro LVM2(gp, varlist, nmeasure, in, Out_Est, Out_Pred);
proc nlmixed data=den_&gp. tech=dbldog qpoints=15 xtol=1e-3;***A minor modification is made to update the covergence criteria in the Safety of Care measure group (XTOL
changed from 1e-4 to 1e-3) to address a warning message in SAS log when using Jan. 2020 data. This modification does not lead to any meaningful changes in measure 
loadings or hospital star ratings, but allows standard errors to be estimated.****************;
ods output convergencestatus=QAcstatus&gp;
parms/data =&in;

array y{*} &varlist;
array mu{*} mu1-mu&nmeasure;
array gamma{*} gamma1-gamma&nmeasure;
array log_den{*} log_den1-log_den&nmeasure;
array w{*} n_w_1-n_w_&nmeasure;
array err{*} err1-err&nmeasure;
	        
logmu = 0; 

do i=1 to &nmeasure; 

	   if y{i} = . then log_den{i}= 0;
	       else   log_den{i} = w{i} * log (pdf('normal', y{i}, mu{i} + gamma{i}*alpha, err{i}));

       logmu = log_den{i} + logmu;
end;

model id ~ general(logmu); 

random alpha ~ normal(0, 1) subject= provider_id;

ods output ParameterEstimates=&Out_Est;
predict alpha out=&Out_Pred; 
run;

/* hospitals with no measures in each domain should have prediction '.'
   predicion 0 is actually due to no measures in the measure group 
   - change it to '.' for the redistributed weights later*/

data &Out_Pred;
set &Out_Pred;
if total_cnt=0 then pred=.;
if total_cnt=0 then lower=.;
if total_cnt=0 then upper=.;
if total_cnt=0 then StdErrPred=.;
run;

	proc print data=QAcstatus&gp noobs;
        title "QA: (&GP.) convergence status";
    run;
	proc freq data=&out_pred;
        title "QA: (&GP.) distribution of PRED";
        tables total_cnt * pred / list missing out=QApreddist_&gp;
        format total_cnt pred pred.;
    run;
    proc print data=&out_est noobs;
        title "QA: (&GP.) Coefficients";
    run;
%mend LVM2;

/* update measures by removing measure with significant negative loading */
%macro LVM(indsn, gp, varlist, nmeasure, Out_Est, Out_Pred);

%LVM_ini(&gp, &varlist, &nmeasure, &Out_Est._ini);

*remove measure(s) with significant negative loading;
data tt(keep=neg);set  &Out_Est._ini;
if substr(parameter,1,5)='gamma' and estimate<0 and probt<0.05;
neg0=substr(parameter,6);
neg=input(neg0,2.0);
run;

PROC SQL; 
select nobs into: nobs from dictionary.tables
where libname="WORK" and memname="TT";
quit;


*when there are measures with significant negative loading; 
%if &nobs ne 0 %then %do;
data tt0;set  &indsn;neg=_N_;run;
data &indsn._new;merge tt(in=a) tt0;by neg;if not a;drop neg;run;

PROC SQL;
select measure_in_std  into: varlist separated by '' notrim
from &indsn._new;
QUIT;

PROC SQL;
select measure_in_den  into: varlist_den separated by '' notrim
from &indsn._new;
QUIT;

PROC SQL;
select count(measure_in_name)
into: nmeasure
from &indsn._new;
QUIT;

*output file with negative loading;
data measure_name(keep=id measure_in_std);
set &indsn;id=_N_;
run;

data est;set &Out_Est._ini;
id=compress(parameter,'','A')*1;
id2=_N_;
run;

proc sql;
create table est22 as
select measure_name.measure_in_std, est.*
from measure_name, est
where measure_name.id=est.id
order by id2;
quit;

data &Out_Est._ini_neg;set est22;
if substr(parameter,1,5)='gamma' then neg_loading=0;
if estimate<0 and probt<0.05 and substr(parameter,1,5)='gamma' then neg_loading=1;
if neg_loading=1 then measure_neg=measure_in_std;
drop id id2 measure_in_std;
run;



%if "&gp" = "PtExp" %then %do;
%LET den_PtExp = H_NUMB_COMP H_RESP_RATE_P; 
%sub_data(&MEASURE_ANALYSIS, &gp, &varlist, &den_PtExp, &nmeasure);
%denominator_PtExp(&gp, &den_PtExp,  &varlist, &nmeasure);
%end;
%else %do;
%sub_data(&MEASURE_ANALYSIS, &gp, &varlist, &varlist_den, &nmeasure);
%denominator(&gp, &varlist_den,  &varlist, &nmeasure);
%end;

/* get initial values of LVM using the updated measures */
%LVM_ini(&gp, &varlist, &nmeasure, &Out_Est._ini);
%if "&gp" = "OS" %then %do;
%LVM2(&gp, &varlist, &nmeasure,&Out_Est._ini, &Out_Est, &Out_Pred);
%end;
%else %do;
%LVM1(&gp, &varlist, &nmeasure,&Out_Est._ini, &Out_Est, &Out_Pred);
%end;

*add measure name for Out_Est output file;
data measure_name(keep=id measure_in_std);
set &indsn._new;id=_N_;
run;

data est;set &Out_Est;
id=compress(parameter,'','A')*1;
id2=_N_;
run;

proc sql;
create table est2 as
select measure_name.measure_in_std, est.*
from measure_name, est
where measure_name.id=est.id
order by id2;
quit;
data &Out_Est;set est2;drop id id2; if substr(parameter,1,5) ne 'gamma' then measure_in_std='';run;

%end;


*when there are NO measures with significant negative loading;
%if &nobs eq 0 %then %do;
%if "&gp" = "OS" %then %do;
%LVM2(&gp, &varlist, &nmeasure,&Out_Est._ini, &Out_Est, &Out_Pred);
%end;
%else %do;
%LVM1(&gp, &varlist, &nmeasure,&Out_Est._ini, &Out_Est, &Out_Pred);
%end;


*add measure name for Out_Est output file;
data measure_name(keep=id measure_in_std);
set &indsn;id=_N_;
run;

data est;set &Out_Est;
id=compress(parameter,'','A')*1;
id2=_N_;
run;

proc sql;
create table est2 as
select measure_name.measure_in_std, est.*
from measure_name, est
where measure_name.id=est.id
order by id2;
quit;
data &Out_Est;set est2;drop id id2; if substr(parameter,1,5) ne 'gamma' then measure_in_std='';run;

%end;

%mend LVM;


*********************************
* macro for reporting indicator *
*********************************;

%macro report(indsn, outdsn, nmeasure_OM, nmeasure_OS, nmeasure_OR, nmeasure_PtExp, nmeasure_effeiciency, nmeasure_ProcessT, nmeasure_ProcessE);

data &outdsn. (keep=provider_id report_indicator Patient_Experience_cnt Outcomes_Readmission_cnt
							Outcomes_Mortality_cnt Outcomes_safety_cnt Efficiency_cnt Process_Timeliness_cnt
							Process_Effectiveness_cnt Total_measure_group_cnt Outcomes_Measure_Group_cnt);

set &indsn.; 

/* Mortality measures*/
array Y_M(&nmeasure_OM.) &measure_OM;
ARRAY M(&nmeasure_OM.) M1-M&nmeasure_OM.;

DO I =1 TO &nmeasure_OM.;
if Y_M[I] ^=. then M[I] =1;
else M[I]=0;
END;

Outcomes_Mortality_cnt=sum (of M1-M&nmeasure_OM.);

/* Saftey measures */
array Y_S(&nmeasure_OS.) &measure_OS;
ARRAY S(&nmeasure_OS.) S1-S&nmeasure_OS.;

DO I =1 TO &nmeasure_OS.;
if Y_S[I] ^=. then S[I] =1;
else S[I]=0;
END;

Outcomes_safety_cnt=sum (of S1-S&nmeasure_OS.);

/* Readmisison measures */
array Y_R(&nmeasure_OR.) &measure_OR;
ARRAY R(&nmeasure_OR.) R1-R&nmeasure_OR.;

DO I =1 TO &nmeasure_OR.;
if Y_R[I] ^=. then R[I] =1;
else R[I]=0;
END;

Outcomes_Readmission_cnt=sum (of R1-R&nmeasure_OR.);

/* Patient Experience measures */
array Y_H(&nmeasure_PtExp.)  &measure_PtExp;
array H(&nmeasure_PtExp.) H1-H&nmeasure_PtExp.;

DO I =1 TO &nmeasure_PtExp.;
if Y_H[I] ^=. then H[I] =1;
else H[I]=0;
END;

Patient_Experience_cnt=sum (of H1-H&nmeasure_PtExp.);

/* Efficiency measures */
array Y_E(&nmeasure_effeiciency.) &measure_Ef;
ARRAY E(&nmeasure_effeiciency.) E1-E&nmeasure_effeiciency.;

DO I =1 TO &nmeasure_effeiciency.;
if Y_E[I] ^=. then E[I] =1;
else E[I]=0;
END;

Efficiency_cnt=sum (of E1-E&nmeasure_effeiciency.);

/* Process Timeliness measures */
array Y_PT(&nmeasure_ProcessT.) &measure_ProcessT;
ARRAY PT(&nmeasure_ProcessT.) PT1-PT&nmeasure_ProcessT.;

DO I =1 TO &nmeasure_ProcessT.;
if Y_PT[I] ^=. then PT[I] =1;
else PT[I]=0;
END;

Process_Timeliness_cnt=sum (of PT1-PT&nmeasure_ProcessT.);

/* Process Effectiveness measures */
array Y_PC(&nmeasure_ProcessE.) &measure_ProcessE;
ARRAY PC(&nmeasure_ProcessE.) PC1-PC&nmeasure_ProcessE.;

DO I =1 TO &nmeasure_ProcessE.;
if Y_PC[I] ^=. then PC[I] =1;
else PC[I]=0;
END;

Process_Effectiveness_cnt =sum (of PC1-PC&nmeasure_ProcessE.);

Array D_cnt(7) Patient_Experience_cnt Outcomes_Mortality_cnt Outcomes_Readmission_cnt
               Outcomes_safety_cnt Efficiency_cnt Process_Timeliness_cnt Process_Effectiveness_cnt;
Array D(7) D1-D7;

DO I =1 TO 7;
if D_cnt[I]>=3 then D[I] =1;
else D[I]=0;
END;

Total_measure_group_cnt =sum (of D1-D7);

Outcomes_Measure_Group_cnt = (Outcomes_Mortality_cnt>=3) + (Outcomes_Readmission_cnt>=3) + (Outcomes_safety_cnt>=3);

report_indicator = (Outcomes_Measure_Group_cnt>=1) and (Total_measure_group_cnt>=3);

run;
%mend;


******************************************
** Macro for 3 Measure Group Categories **
******************************************;
%macro group_3cat(indata=, out=);
   data temp;
	   set &indata;
	   where total_cnt>=3;*missing(pred)^=1;
	   keep provider_id total_cnt pred stderrPred lower upper;
	run;

	proc sort data=temp; by provider_id; run;

	proc sql;
	   select mean(pred) into :ori_mean from temp;
	quit;

	data &out;
	   set temp;
	   length ori_cat $30;

	   if lower>&ori_mean>. then ori_cat='Above the national average';
       else if .<upper<&ori_mean then ori_cat='Below the national average';
       else if lower<=&ori_mean<=upper then ori_cat='Same as the national average';
       else ori_cat = 'ERROR';               
    run;
    proc print data=&out noobs;
        title "QA: (&out.) Error coding ORI_MEAN";
        where ori_cat = "ERROR";
    run;
    title " ";
%mend;


******************************************
** Macro for national average           **
******************************************;
%macro nation_avg0(indata=, out=, var=);
   data temp;
	   set &indata;
	   where report_indicator=1;**********;
	   keep provider_id summary_score;
	run;

	proc sql;
	   create table out1 as
	   select mean(summary_score) as &var from temp;
	quit;
    
	data &out(keep=&var);set out1;run;
%mend;

%macro nation_avg(indata=, out=, var=);
   data temp;
	   set &indata;
	   where total_cnt>=3;**********;
	   keep provider_id total_cnt pred stderrPred lower upper;
	run;

	proc sql;
	   create table out1 as
	   select mean(pred) as &var from temp;
	quit;

	data &out(keep=&var);set out1;run;
%mend;
