/******************************************************************************/
/* 	Produces results based on the Monte Carlo simulations
	
	2020-11-25
	Version 1.0
*/

/******************************************************************************/
* Change directory
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"

/******************************************************************************/
/* Results */

/* Differences */
tempfile f0 f1 f2 f3 

use data/cms_weights_std0_grp0, clear
gen diff0 = ub_rank-lb_rank
summ diff0
rename mean_rank mean0
keep provider_id mean0 diff0

label var mean0 "Mean rank, changing weights only"
label var diff0 "Rank IQR, changing weights only"
save `f0'

use data/cms_weights_std0_grp1, clear
gen diff1 = ub_rank-lb_rank
summ diff1
rename mean_rank mean1
keep provider_id mean1 diff1

label var mean1 "Mean rank, changing weights and groups"
label var diff1 "Rank IQR, changing weights and groups"
save `f1'

use data/cms_weights_std1_grp0, clear
gen diff2 = ub_rank-lb_rank
summ diff2
rename mean_rank mean2
keep provider_id mean2 diff2

label var mean2 "Mean rank, changing weights and standardisation"
label var diff2 "Rank IQR, changing weights and standardisation"
save `f2'

use data/cms_weights_std1_grp1, clear
gen diff3 = ub_rank-lb_rank
summ diff3
rename mean_rank mean3
keep provider_id mean3 diff3

label var mean3 "Mean rank, changing weights, groups and standardisation"
label var diff3 "Rank IQR, changing weights, groups and standardisation"
save `f3'

use `f0', clear
merge 1:1 provider_id using `f1', assert(3) nogenerate
merge 1:1 provider_id using `f2', assert(3) nogenerate
merge 1:1 provider_id using `f3', assert(3) nogenerate

summ diff0 diff1 diff2 diff3

reshape long diff mean, i(provider_id) j(type1)

replace mean = 3726-mean+1

#delimit ;
label define type1	0 "A. Weights only"
					1 "B. Weights and grouping"
					2 "C. Weights and standardisation"
					3 "D. Weights, grouping and standardisation"
					, replace
					;
#delimit cr
label values type1 type1

#delimit ;
twoway	(scatter diff mean, msymb(oh) mcolor(purple%20) )
		(lowess diff mean, lc(gs0) lw(*2) )
		(scatter diff mean if diff > 1000000, msymb(oh) mcolor(purple) )
		, 	by(
				type1
				,	note("")
			)
			xlabel(200 "1 (Best)" 3526 "Worst", tl(0))
			ylabel(1 3726 , tl(0) angle(h) )	
			ytitle("Difference between 25th and 75th percentile of ranks")
			xtitle("Average rank across 10,000 simulations")
			legend(
				order(
					3 "Individual hospital"
					2 "Lowess"
				)
				region(lstyle(none))
			)
			name(diff_v_mean, replace)
		;
#delimit cr


#delimit ;
hist diff
	,	by(
			type1
			, 	note("")
		)
		subtitle(, fcolor(none) pos(11) nobox)
		xlabel(1 1000 2000 3000 3726)
		xtitle("Difference between 25th and 75th percentile of ranks")
		freq
	;
#delimit cr
graph export results/figure8_final_cms_diffs_histogram.png, width(1000) replace		

* DIFFS AGAIN
bys type1: summ diff

/* Plots */
* Weights only
use "data/approaches_combined", clear
keep provider_id std1_grp1_wt1_rank

merge 1:1 provider_id using data/cms_weights_std0_grp0, assert(1 3)
drop if _merge == 1
drop _merge

local xtitle "Average rank across 10,000 simulations"
local xtitle "Rank on current CMS approach"

count
foreach thing in mean_rank min_rank lb_rank ub_rank max_rank {
	* make lower ranks = better performance
	replace `thing' = r(N)-`thing'+1
	
}

#delimit ;
twoway 	(rspike lb_rank ub_rank mean_rank, lc(purple%20) ) 
		(scatter mean_rank mean_rank, msymb(p) mc(purple%80) )
		,	legend(off)
			ytitle("25th to 75th percentile of ranks")
			xtitle("`xtitle'")
			xsc(r(1 1000)) ysc(r(1 1000))
			xlabel(1 "Best" 1000 2000 3000 3726 "Worst")
			ylabel(1 "Best" 1000(1000)3000 3726 "Worst", angle(h) grid tl(0) )
			title(
				"A. Uncertainty in ranks of individual hospitals from choice of weights" 
				" "
				, size(medsmall) pos(11)
			)
			name(ranks0, replace)
		;
#delimit cr

* Weights and groups
use "data/approaches_combined", clear
keep provider_id std1_grp1_wt1_rank

merge 1:1 provider_id using data/cms_weights_std0_grp1, assert(1 3)
drop if _merge == 1
drop _merge


count
foreach thing in mean_rank min_rank lb_rank ub_rank max_rank {
	* make lower ranks = better performance
	replace `thing' = r(N)-`thing'+1
	
}

scatter lb_rank ub_rank, msymb(oh) mc(purple%80)


#delimit ;
twoway 	(rspike lb_rank ub_rank mean_rank, lc(purple%20) ) 
		(scatter mean_rank mean_rank, msymb(p) mc(purple%80) )
		,	legend(off)
			ytitle("25th to 75th percentile of ranks")
			xtitle("`xtitle'")
			xsc(r(1 1000)) ysc(r(1 1000))
			xlabel(1 "Best" 1000 2000 3000 3726 "Worst")
			ylabel(1 "Best" 1000(1000)3000 3726 "Worst", angle(h) grid tl(0) )
			title(
				"B. Uncertainty in ranks of individual hospitals from choice of weights" 
				"and approach to grouping"
				, size(medsmall) pos(11)
			)
			name(ranks1, replace)
		;
#delimit cr

* Weights and standardisation
use "data/approaches_combined", clear
keep provider_id std1_grp1_wt1_rank

merge 1:1 provider_id using data/cms_weights_std1_grp0, assert(1 3)
drop if _merge == 1
drop _merge


count
foreach thing in mean_rank min_rank lb_rank ub_rank max_rank {
	* make lower ranks = better performance
	replace `thing' = r(N)-`thing'+1
	
}


#delimit ;
twoway 	(rspike lb_rank ub_rank mean_rank, lc(purple%20) ) 
		(scatter mean_rank mean_rank, msymb(p) mc(purple%80) )
		,	legend(off)
			ytitle("25th to 75th percentile of ranks")
			xtitle("`xtitle'")
			xsc(r(1 1000)) ysc(r(1 1000))
			xlabel(1 "Best" 1000 2000 3000 3726 "Worst")
			ylabel(1 "Best" 1000(1000)3000 3726 "Worst", angle(h) grid tl(0) )
			title(
				"C. Uncertainty in ranks of individual hospitals from choice of weights" 
				"and approach to standardisation"
				, size(medsmall) pos(11)
			)
			name(ranks2, replace)
		;
#delimit cr

* Weights groups and standardisation
use "data/approaches_combined", clear
keep provider_id std1_grp1_wt1_rank

merge 1:1 provider_id using data/cms_weights_std1_grp1, assert(1 3)
drop if _merge == 1
drop _merge

count
foreach thing in mean_rank min_rank lb_rank ub_rank max_rank {
	* make lower ranks = better performance
	replace `thing' = r(N)-`thing'+1
	
}

#delimit ;
twoway 	(rspike lb_rank ub_rank mean_rank, lc(purple%20) ) 
		(scatter mean_rank mean_rank, msymb(p) mc(purple%80) )
		,	legend(off)
			ytitle("25th to 75th percentile of ranks")
			xtitle("`xtitle'")
			xsc(r(1 1000)) ysc(r(1 1000))
			xlabel(1 "Best" 1000 2000 3000 3726 "Worst")
			ylabel(1 "Best" 1000(1000)3000 3726 "Worst", angle(h) grid tl(0) )
			title(
				"D. Uncertainty in ranks of individual hospitals from choice of weights" 
				"and approach to both grouping and standardisation"
				, size(medsmall) pos(11)
			)
			name(ranks3, replace)
		;
#delimit cr

/* COMBINE */
graph combine ranks0 ranks1 ranks2 ranks3, cols(1) ysize(8) xsize(4) name(ranks_combi_original, replace)
graph export results/figure7_CMS_MonteCarlo_SummaryPlot.png, width(1000) replace
