/******************************************************************************/
/* 	Group
	
	Original groups are well-defined. This uses factor analysis (ML) to identify 
	groups from the Z-scored and reference-grouped data.
	
	This skips the EFA and just uses groups from the "known" results.
	
	2020-11-25
	Version 1.0
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"


/******************************************************************************/
/* Find groups in Z-scored data 											  */
use data/std_method1.dta, clear

keep 	provider_id /// 
		mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??

* F1 - all patient experience measures
*	safety_2 (CLABSI) could be f1 or f2. As not patient experience, assigned to f2.
*	effec_2 (staff flu vaccination) could be f1 or f4. As not pat ex, assigned to f4.
* 	effec_8 (radiation for bone mets) could be f1 or f4. As not pat ex, assigned to f4.
rename (ptexp_? ptexp_??) f1_=

* F2 - safe, efficient care
/*
timely_1        float   %8.0g                 ED - time arrival to departure
timely_2        float   %8.0g                 ED - time admit decision to departure
timely_7        float   %8.0g                 ED - time arrival to discharge
effec_4         byte    %12.0g                % patients leaving ED unseen
effic_5         double  %12.0g                Brain and sinus CT
safety_8        double  %12.0g                PSI-90
readm_3         double  %12.0g                Excess days in acute care PN
safety_5        double  %12.0g                SSI Hysterectomy
timely_6        float   %8.0g                 OP - time to ECG
effic_1         double  %12.0g                MRI lumbar spine for low back pain
safety_3        double  %12.0g                CAUTI
safety_4        double  %12.0g                SSI Colon
safety_2        double  %12.0g                CLABSI
readm_4         double  %12.0g                Hospital visits after OP colonoscopy
*/
#delimit ;
rename (
		timely_1
		timely_2
		timely_7
		effec_4 
		effic_5 
		safety_8
		readm_3 
		safety_5
		timely_6
		/*effic_1*/
		safety_3
		safety_4
		safety_2
		/*readm_4*/ 
	) f2_= ;
#delimit cr
rename (effic_1 readm_4) f2_negative_=
replace f2_negative_effic_1 = -f2_negative_effic_1
replace f2_negative_readm_4 = -f2_negative_readm_4

* F3 - Readmission
/*
readm_8         double  %12.0g                Hospital-wide readmission
readm_2         double  %12.0g                Excess days in acute care HF
readm_6         double  %12.0g                Readmission following COPD
readm_1         double  %12.0g                Excess days in acute care AMI
readm_5         double  %12.0g                Readmission following CABG
readm_7         double  %12.0g                Readmission following hip and knee surgery
safety_1        double  %12.0g                Hip and knee surgery complications
safety_6        double  %12.0g                MRSA
*/
#delimit ;
rename (
		readm_8
		readm_2
		readm_6
		readm_1
		readm_5
		readm_7
		safety_1
		safety_6
	) f3_= ;
#delimit cr

* F4 - Effective clinical practice
/*
effec_1         double  %12.0g                Flu vaccinations
effec_5         double  %12.0g                Stroke - brain scan within 45 mins
effec_10        double  %12.0g                % approp care for sepsis
effec_7         double  %12.0g                % Hx polyps receiving follow-up colonsc
effec_11        double  %12.0g                % blood clot while no prevention
effec_6         double  %12.0g                % receiving approp follow-up after colonsc
effec_9         byte    %12.0g                % mothers delivering early unnec
effic_3         double  %12.0g                Thorax CT
effec_8         double  %12.0g                % radiation therapy for bone mets
effic_2         double  %12.0g                Abdomen CT
effec_2         float   %9.0g                 Staff flu vaccinations
effic_4         double  %12.0g                Cardiac imaging for preop risk assess
*/
#delimit ;
rename (
		effec_1 
		effec_5 
		effec_10
		effec_7 
		effec_11
		effec_6 
		effec_9 
		effic_3 
		effec_8 
		effic_2 
		effec_2 
		/*effic_4*/
	) f4_= ;
#delimit cr
rename effic_4 f4_negative_effic_4
replace f4_negative_effic_4 = -f4_negative_effic_4

* F5 - mortality
/*
mortality_4     double  %12.0g                30-day mortality from HF
mortality_5     double  %12.0g                30-day mortality from PN
mortality_3     double  %12.0g                30-day mortality from COPD
mortality_1     double  %12.0g                30-day mortality from AMI
mortality_6     double  %12.0g                30-day mortality from STK
mortality_2     double  %12.0g                30-day mortality from CABG
mortality_7     double  %12.0g                30-day mortality from surg comps
*/
rename (mortality_*) f5_=

* F6 - specialist care availability ?
/*
timely_5        float   %8.0g                 OP - time to specialist care
safety_7        double  %12.0g                C diff
*/
rename (timely_5 safety_7) f6_=

* based on hospital compare, only calculate group score if 3+ measures
* (do not apply to f6, as only two measures)
forval i = 1/5 {
	egen nm_f`i' = rownonmiss(f`i'_*)
	foreach var of varlist f`i'_* {
		replace `var' = . if nm_f`i' < 3
	}
}

* generate group summary scores
/*
egen f1 = rowmean(f1_*)
egen f2 = rowmean(f2_*)
egen f3 = rowmean(f3_*)
egen f4 = rowmean(f4_*)
egen f5 = rowmean (f5_*)
*/

* Use SEM to fit a measurement model where practical
sem (f1_* <- F1) if nm_f1 >= 3, method(mlmv)
predict f1 if nm_f1 >= 3, latent(F1)

sem (f2_* <- F2) if nm_f2 >= 3, method(mlmv)
predict f2 if nm_f2 >= 3, latent(F2)

sem (f3_* <- F3) if nm_f3 >= 3, method(mlmv)
predict f3 if nm_f3 >= 3, latent(F3)

sem (f4_* <- F4) if nm_f4 >= 3, method(mlmv)
predict f4 if nm_f4 >= 3, latent(F4)

sem (f5_* <- F5) if nm_f5 >= 3, method(mlmv)
predict f5 if nm_f5 >= 3, latent(F5)

* Only two variables for F6 - just take a mean
egen f6 = rowmean (f6_*)

* How shall we think of these factors?
desc f1_*
desc f2_*
desc f3_*
desc f4_*
desc f5_*
desc f6_*

label var f1 "Patient experience"
label var f2 "Safe, efficient care" // reflects ED? Operational safety?
label var f3 "Readmissions and complications"
label var f4 "Effective clinical practice"
label var f5 "Mortality"
label var f6 "Specialist care availability"

keep provider_id f1-f6

label data "Standardisation by Z-scores, group by EFA"
save data/std_method1_grp_efa.dta, replace

/******************************************************************************/
/* Find groups in reference-standardised data								  */
use data/std_method2.dta, clear

keep 	provider_id /// 
		mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??

* see excel file for details of factor assignments

* G1 - all patient experience measures
*	safety_2 (CLABSI) could be g1 or f2. As not patient experience, assigned to f2.
*	effec_2 (staff flu vaccination) could be f1 or f4. As not pat ex, assigned to f4.
* 	effec_8 (radiation for bone mets) could be f1 or f4. As not pat ex, assigned to f4.
rename (ptexp_? ptexp_??) g1_=

* G2 - safe, efficient care
/*
timely_1        float   %8.0g                 ED - time arrival to departure
timely_2        float   %8.0g                 ED - time admit decision to departure
timely_7        float   %8.0g                 ED - time arrival to discharge
effec_4         byte    %12.0g                % patients leaving ED unseen
safety_8        double  %12.0g                PSI-90
safety_5        double  %12.0g                SSI Hysterectomy
safety_3        double  %12.0g                CAUTI
safety_4        double  %12.0g                SSI Colon
*/
#delimit ;
rename (
		timely_1
		timely_2
		timely_7
		effec_4 
		safety_8
		safety_5
		safety_3
		safety_4		
	) g2_= ;
#delimit cr

* G3 - Readmission
/*
readm_8         double  %12.0g                Hospital-wide readmission
readm_2         double  %12.0g                Excess days in acute care HF
readm_6         double  %12.0g                Readmission following COPD
readm_1         double  %12.0g                Excess days in acute care AMI
readm_3
readm_5         double  %12.0g                Readmission following CABG
readm_7         double  %12.0g                Readmission following hip and knee surgery
safety_1        double  %12.0g                Hip and knee surgery complications
safety_6        double  %12.0g                MRSA
*/
#delimit ;
rename (
		readm_8
		readm_2
		readm_6
		readm_1
		readm_3
		readm_5
		readm_7
		safety_1
		safety_6
	) g3_= ;
#delimit cr

* G4 - Effective clinical practice
/*
effec_1         double  %12.0g                Flu vaccinations
effec_5         double  %12.0g                Stroke - brain scan within 45 mins
effec_11        double  %12.0g                % blood clot while no prevention
effec_10        double  %12.0g                % approp care for sepsis
effec_7         double  %12.0g                % Hx polyps receiving follow-up colonsc
effec_6         double  %12.0g                % receiving approp follow-up after colonsc
timely_6        float   %8.0g                 OP - time to ECG
effec_9         byte    %12.0g                % mothers delivering early unnec
effic_3         double  %12.0g                Thorax CT
effec_8         double  %12.0g                % radiation therapy for bone mets
effic_2         double  %12.0g                Abdomen CT
effec_2         float   %9.0g                 Staff flu vaccinations
effic_1         double  %12.0g                MRI lumbar spine for low back pain
safety_2        double  %12.0g                CLABSI
effic_4         double  %12.0g                Cardiac imaging for preop risk assess
readm_4         double  %12.0g                Hospital visits after OP colonoscopy
safety_7        double  %12.0g                C diff
effic_5         double  %12.0g                Brain and sinus CT
*/
#delimit ;
rename (
		effec_1 
		effec_5 
		effec_11
		effec_10
		effec_7 
		effec_6 
		timely_6
		effec_9 
		effic_3 
		effec_8 
		effic_2 
		effec_2 
		effic_1 
		safety_2
		/*effic_4*/ 
		readm_4 
		safety_7
		/*effic_5*/ 
	) g4_= ;
#delimit cr
rename (effic_4 effic_5) g4_negative_=
replace g4_negative_effic_4 = -g4_negative_effic_4
replace g4_negative_effic_5 = -g4_negative_effic_5

* G5 - mortality
/*
mortality_4     double  %12.0g                30-day mortality from HF
mortality_5     double  %12.0g                30-day mortality from PN
mortality_3     double  %12.0g                30-day mortality from COPD
mortality_1     double  %12.0g                30-day mortality from AMI
mortality_6     double  %12.0g                30-day mortality from STK
mortality_2     double  %12.0g                30-day mortality from CABG
mortality_7     double  %12.0g                30-day mortality from surg comps
*/
rename (mortality_*) g5_=

* G6 - specialist care availability ?
/*
timely_5        float   %8.0g                 OP - time to specialist care
safety_7        double  %12.0g                C diff
*/
rename (timely_5) g6_=


* based on hospital compare, only calculate group score if 3+ measures
* (do not apply to g6, as only one measure)
forval i = 1/5 {
	egen nm_g`i' = rownonmiss(g`i'_*)
	foreach var of varlist g`i'_* {
		replace `var' = . if nm_g`i' < 3
	}
	drop nm_g`i'
}

* generate group summary scores
egen g1 = rowmean(g1_*)
egen g2 = rowmean(g2_*)
egen g3 = rowmean(g3_*)
egen g4 = rowmean(g4_*)
egen g5 = rowmean(g5_*)
egen g6 = rowmean(g6_*)

* How shall we think of these factors?
desc g1_*
desc g2_*
desc g3_*
desc g4_*
desc g5_*
desc g6_*

label var g1 "Patient experience"
label var g2 "Safe, efficient care" // reflects ED? Operational safety?
label var g3 "Readmissions and complications"
label var g4 "Effective clinical practice"
label var g5 "Mortality"
label var g6 "Specialist care availability"

keep provider_id g1-g6

label data "Standardisation by reference, group by EFA"
save data/std_method2_grp_efa.dta, replace
 

/******************************************************************************/
/* Assign groups in Z-scored data 											  */
use data/std_method1.dta, clear

keep 	provider_id /// 
		mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??
		
* based on hospital compare, only calculate group score if 3+ measures
foreach group in mortality safety readm ptexp effic timely effec {
	egen nm_`group' = rownonmiss(`group'_*)
	foreach var of varlist `group'_* {
		replace `var' = . if nm_`group' < 3
	}
}

sem (mortality_? <- Mortality) if nm_mortality >= 3, method(mlmv)
predict mortality_m1 if nm_mortality >= 3, latent(Mortality)

sem (safety_? <- Safety) if nm_safety >= 3, method(mlmv)
predict safety_m1 if nm_safety >= 3, latent(Safety)

sem (readm_? <- Readm) if nm_readm >= 3, method(mlmv)
predict readm_m1 if nm_readm >= 3, latent(Readm)

sem (ptexp_? ptexp_?? <- Ptexp) if nm_ptexp >= 3, method(mlmv)
predict ptexp_m1 if nm_ptexp >= 3, latent(Ptexp)

sem (effic_? <- Effic) if nm_effic >= 3, method(mlmv)
predict effic_m1 if nm_effic >= 3, latent(Effic)

* MODEL DOES NOT CONVERGE
* sem (timely_m1 <- Timely), method(mlmv)
egen timely_m1 		= rowmean(timely_?)

sem (effec_? effec_?? <- Effec) if nm_effec >= 3, method(mlmv)
predict effec_m1 if nm_effec >= 3, latent(Effec)

keep provider_id mortality_m1-effec_m1

label data "Standardisation by Z-score, group by policy"
save data/std_method1_grp_pol.dta, replace

/******************************************************************************/
/* Assign groups in reference data 											  */
use data/std_method2.dta, clear

keep 	provider_id /// 
		mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??

* based on hospital compare, only calculate group score if 3+ measures
foreach group in mortality safety readm ptexp effic timely effec {
	egen nm_`group' = rownonmiss(`group'_*)
	foreach var of varlist `group'_* {
		replace `var' = . if nm_`group' < 3
	}
	drop nm_`group' 
}
	
egen mortality_m2 	= rowmean(mortality_?)
egen safety_m2		= rowmean(safety_?)
egen readm_m2 		= rowmean(readm_?)
egen ptexp_m2 		= rowmean(ptexp_? ptexp_??)
egen effic_m2 		= rowmean(effic_?)
egen timely_m2 		= rowmean(timely_?)
egen effec_m2 		= rowmean(effec_? effec_??)

keep provider_id mortality_m2-effec_m2

label data "Standardisation by reference, group by policy"
save data/std_method2_grp_pol.dta, replace 
 
