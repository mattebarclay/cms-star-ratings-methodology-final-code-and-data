/******************************************************************************/
/* 	Produces the results plots etc based on the datasets produced in Combine
	
	2020-11-25
	Version 1.0
*/

/******************************************************************************/
* Change directory
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"

/******************************************************************************/
* Load data
use data/approaches_combined, clear
 
 
/******************************************************************************/
* Program to make graphs

cap program drop make_graphs
program define make_graphs
	syntax , changed(string) title1(string) title2(string) nameit(string)

	forval i = 1/5 {
		summ `changed'_rank if `changed'_stars == `i', meanonly
		local ymax`i' = r(max)
		local ymin`i' = r(min)
		
		summ std1_grp1_wt1_rank if std1_grp1_wt1_stars == `i', meanonly
		local xmax`i' = r(max)
		local xmin`i' = r(min)
	}

	forval i = 1/4 {
		local yline`i' = (`ymax`i''+`ymin`=`i'+1'')/2
		local xline`i' = (`xmax`i''+`xmin`=`i'+1'')/2
	}

	#delimit ;
	twoway (
			scatter `changed'_rank std1_grp1_wt1_rank 
				if (grp1_cat == 0)
			,	msymb(oh)
				mcolor(green%20)
		)
		(
			scatter `changed'_rank std1_grp1_wt1_rank 
				if (grp1_cat == 1)
			,	msymb(oh)
				mcolor(blue%20)
		)
		(
			scatter `changed'_rank std1_grp1_wt1_rank 
				if (grp1_cat == 2)
			,	msymb(oh)
				mcolor(purple%20)
		)
		(
			scatter `changed'_rank std1_grp1_wt1_rank 
				if (grp1_cat == 3)
			,	msymb(oh)
				mcolor(red%20)
		)
		,	ytitle("`title2'")
			xtitle("Current approach")
			title("`title1'", span pos(11))
			ysc(noextend) 
			plotregion(lstyle(none))
			xsc(noextend)
			ylabel(
				`=1+`yline4'/2'						"5 star" 
				`=`yline4'+(`yline3'-`yline4')/2' 	"4 star" 
				`=`yline3'+(`yline2'-`yline3')/2' 	"3 star" 
				`=`yline2'+(`yline1'-`yline2')/2' 	"2 star" 
				`=`yline1'+(3725-`yline1')/2' 		"1 star"
				, angle(h) tl(0) 
			)
			xlabel(
				`=1+`xline4'/2'						"5 star" 
				`=`xline4'+(`xline3'-`xline4')/2' 	"4 star" 
				`=`xline3'+(`xline2'-`xline3')/2' 	"3 star" 
				`=`xline2'+(`xline1'-`xline2')/2' 	"2 star" 
				`=`xline1'+(3725-`xline1')/2' 		"1 star"
				, angle(h) tl(0) 
			)
			ytick(1 `yline1' `yline2' `yline3' `yline4' 3725)
			xtick(1 `xline1' `xline2' `xline3' `xline4' 3725)
			legend(off)
			yline(`yline1' `yline2' `yline3' `yline4', lcolor(gs8) lpattern(shortdash) noextend) 
			xline(`xline1' `xline2' `xline3' `xline4', lcolor(gs8) lpattern(shortdash) noextend)
			name("`nameit'", replace)
		;

	#delimit cr
end

/******************************************************************************/
/* Programs to calculate numbers for tables */

* Kendall's tau correlations for different specifications and subgroups
cap program drop ktau_ing
program define ktau_ing
	syntax , std(int) grp(int) wt(int) output(string)
		
	ktau std`std'_grp`grp'_wt`wt'_rank std1_grp1_wt1_rank 
	local tau_all = r(tau_a)
	
	ktau std`std'_grp`grp'_wt`wt'_rank std1_grp1_wt1_rank  if grp1_cat == 0
	local tau_0 = r(tau_a)
	
	ktau std`std'_grp`grp'_wt`wt'_rank std1_grp1_wt1_rank  if inlist(grp1_cat,1,2,3)
	local tau_1 = r(tau_a)
	
	ktau std`std'_grp`grp'_wt`wt'_rank std1_grp1_wt1_rank  if grp1_cat == 1
	local tau_1a = r(tau_a)
	
	ktau std`std'_grp`grp'_wt`wt'_rank std1_grp1_wt1_rank  if grp1_cat == 2
	local tau_1b = r(tau_a)
	
	ktau std`std'_grp`grp'_wt`wt'_rank std1_grp1_wt1_rank  if grp1_cat == 3
	local tau_1c = r(tau_a)
	
	matrix `output' = (`tau_all', ., `tau_0', ., `tau_1', ., `tau_1a', ., `tau_1b', ., `tau_1c', . )
end

* Changes from 5 stars to 1 star or 1 star to 5 stars (not used in thesis text)
cap program drop count_ing
program define count_ing
	syntax , std(int) grp(int) wt(int) move(real) output(string)
	
	count if abs(std1_grp1_wt1_stars-std`std'_grp`grp'_wt`wt'_stars) == `move' &					 		!missing(std1_grp1_wt1_stars) & !missing(std`std'_grp`grp'_wt`wt'_stars)
	local n_all = r(N)
	
	count if abs(std1_grp1_wt1_stars-std`std'_grp`grp'_wt`wt'_stars) == `move' & grp1_cat == 0 & 			!missing(std1_grp1_wt1_stars) & !missing(std`std'_grp`grp'_wt`wt'_stars)
	local n_0 = r(N)
	
	count if abs(std1_grp1_wt1_stars-std`std'_grp`grp'_wt`wt'_stars) == `move' & inlist(grp1_cat,1,2,3) & !missing(std1_grp1_wt1_stars) & !missing(std`std'_grp`grp'_wt`wt'_stars)
	local n_1 = r(N)
	
	count if abs(std1_grp1_wt1_stars-std`std'_grp`grp'_wt`wt'_stars) == `move' & grp1_cat == 1 & 			!missing(std1_grp1_wt1_stars) & !missing(std`std'_grp`grp'_wt`wt'_stars)
	local n_1a = r(N)

	count if abs(std1_grp1_wt1_stars-std`std'_grp`grp'_wt`wt'_stars) == `move' & grp1_cat == 2 & 			!missing(std1_grp1_wt1_stars) & !missing(std`std'_grp`grp'_wt`wt'_stars)
	local n_1b = r(N)
	
	count if abs(std1_grp1_wt1_stars-std`std'_grp`grp'_wt`wt'_stars) == `move' & grp1_cat == 3 & 			!missing(std1_grp1_wt1_stars) & !missing(std`std'_grp`grp'_wt`wt'_stars)
	local n_1c = r(N)
	
	matrix `output' = (`n_all', ., `n_0', ., `n_1', ., `n_1a', ., `n_1b', ., `n_1c', . )
end

* Changes from 4/5 stars to 1/2 stars or 1/2 stars to 4/5 stars
cap program drop count2_ing
program define count2_ing
	syntax , std(int) grp(int) wt(int) output(string)
	
	#delimit ;
	count if
		(
				(inlist(std1_grp1_wt1_stars,1,2) & inlist(std`std'_grp`grp'_wt`wt'_stars,4,5)) 
			| 	(inlist(std1_grp1_wt1_stars,4,5) & inlist(std`std'_grp`grp'_wt`wt'_stars,1,2))
		)
		;
	local n_all = r(N);
	
	count if 
		(
				(inlist(std1_grp1_wt1_stars,1,2) & inlist(std`std'_grp`grp'_wt`wt'_stars,4,5)) 
			| 	(inlist(std1_grp1_wt1_stars,4,5) & inlist(std`std'_grp`grp'_wt`wt'_stars,1,2))
		)
		& grp1_cat == 0 
		;
	local n_0 = r(N);
	
	count if 
		(
				(inlist(std1_grp1_wt1_stars,1,2) & inlist(std`std'_grp`grp'_wt`wt'_stars,4,5)) 
			| 	(inlist(std1_grp1_wt1_stars,4,5) & inlist(std`std'_grp`grp'_wt`wt'_stars,1,2))
		)
		& inlist(grp1_cat,1,2,3) 
		;
	local n_1 = r(N);
	
	count if 
		(
				(inlist(std1_grp1_wt1_stars,1,2) & inlist(std`std'_grp`grp'_wt`wt'_stars,4,5)) 
			| 	(inlist(std1_grp1_wt1_stars,4,5) & inlist(std`std'_grp`grp'_wt`wt'_stars,1,2))
		)
		& grp1_cat == 1
		;
	local n_1a = r(N);

	count if 
		(
				(inlist(std1_grp1_wt1_stars,1,2) & inlist(std`std'_grp`grp'_wt`wt'_stars,4,5)) 
			| 	(inlist(std1_grp1_wt1_stars,4,5) & inlist(std`std'_grp`grp'_wt`wt'_stars,1,2))
		)
		& grp1_cat == 2
		;
	local n_1b = r(N);
	
	count if 
		(
				(inlist(std1_grp1_wt1_stars,1,2) & inlist(std`std'_grp`grp'_wt`wt'_stars,4,5)) 
			| 	(inlist(std1_grp1_wt1_stars,4,5) & inlist(std`std'_grp`grp'_wt`wt'_stars,1,2))
		)
		& grp1_cat == 3
		;
	local n_1c = r(N);
	
	#delimit cr
	
	matrix `output' = (`n_all', ., `n_0', ., `n_1', ., `n_1a', ., `n_1b', ., `n_1c', . )
end


/******************************************************************************/
* Matrix plot
desc *_rank

gen rank0 = std1_grp1_wt1_rank
label var rank0 "Current CMS approach"

gen rank1 = std1_grp1_wt2_rank
label var rank1 "Alternate weights"

gen rank2 = std1_grp2_wt1_rank
label var rank2 "Alternate domains"

gen rank3 = std2_grp1_wt1_rank
label var rank3 "Alternate standardisation"

gen rank4 = std1_grp2_wt2_rank
label var rank4 "Alternate weights and domains"

gen rank5 = std2_grp1_wt2_rank
label var rank5 "Alternate weights and standardisation"

gen rank6 = std2_grp2_wt1_rank
label var rank6 "Alternate domains and standardisation"

gen rank7 = std2_grp2_wt2_rank
label var rank7 "Alternate weights, domains and standardisation"


#delimit ;
graph matrix rank?
	, 	msymb(oh) mcolor(purple%5) 
		half
		title("", size(medium))
		maxes(
			xtick(0(1000)4000)
			ytick(0(1000)4000)
			ysc(r(-500 4500))
			xsc(r(-500 4500))
		)
		diagopts(size(small))
		ysize(4)
		xsize(4)
;
#delimit cr
graph export "results/exploratory/composite_scenario_cms_correlations_2020-11-16.png", width(1000) replace

/******************************************************************************/
* Graph of single changes vs current CMS Approach
* Actual vs Reference standardisation	
make_graphs, 	changed(std2_grp1_wt1) ///
				title1("C. Only changing approach to standardising measures.") ///
				title2("Absolute standardisation") ///
				nameit("f4_panel_c")

* Actual vs EFA domains
make_graphs, 	changed(std1_grp2_wt1) ///
				title1("B. Only changing approach to grouping measures.") ///
				title2("Domains from EFA") ///
				nameit("f4_panel_b")
	
* Actual vs equal weights
make_graphs, 	changed(std1_grp2_wt1) ///
				title1("B. Only changing weights given to measure domains.") ///
				title2("Equal weights") ///
				nameit("f4_panel_a")
		
* A legend
#delimit ;
twoway 	(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(green)) 
		(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(blue))
		(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(purple))
		(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(red))
		,	legend(
				ring(0) pos(0) cols(2) region(lstyle(none))
				order(
					1 "All seven domains reported"
					2 "Six domains reported"
					3 "Five domains reported"
					4 "Three or four domains reported"
				)
			)
			title("") ytitle("") xtitle("")
			xlabel() ylabel()
			xsc(noline) ysc(noline) plotregion(lstyle(none) margin(0 0 0 0))
			graphregion(margin(0 0 0 0))
			fysize(10)
			name(f4_legend, replace)
		;
#delimit cr

graph combine f4_panel_a f4_panel_b f4_panel_c f4_legend, cols(1) ysize(10)
graph export "results/figure4_CMS_single_changes.png", width(1000) replace 

/******************************************************************************/
* Graph of multiple changes vs current CMS Approach
make_graphs, 	changed(std1_grp2_wt2) ///
				title1("A. Changing weights and domains") ///
				title2("Alternate") ///
				nameit("f6_panel_a")

make_graphs, 	changed(std2_grp1_wt2) ///
				title1("B. Changing weights and standardisation") ///
				title2("Alternate") ///
				nameit("f6_panel_b")

make_graphs, 	changed(std2_grp2_wt1) ///
				title1("C. Changing domains and standardisation") ///
				title2("Alternate") ///
				nameit("f6_panel_c")

make_graphs, 	changed(std2_grp2_wt2) ///
				title1("D. Changing weights, domains and standardisaion") ///
				title2("Alternate") ///
				nameit("f6_panel_d")
				
* A legend
#delimit ;
twoway 	(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(green)) 
		(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(blue))
		(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(purple))
		(scatter std1_grp1_wt1_rank std1_grp1_wt1_rank if 1 == 2, msymb(oh) mcolor(red))
		,	legend(
				ring(0) pos(0) cols(2) region(lstyle(none))
				order(
					1 "All seven domains reported"
					2 "Six domains reported"
					3 "Five domains reported"
					4 "Three or four domains reported"
				)
			)
			title("") ytitle("") xtitle("")
			xlabel() ylabel()
			xsc(noline) ysc(noline) plotregion(lstyle(none) margin(0 0 0 0))
			graphregion(margin(0 0 0 0))
			fysize(10)
			name(f6_legend, replace)
		;
#delimit cr

graph combine f6_panel_a f6_panel_b f6_panel_c f6_panel_d, cols(2) altshrink name(figure6_plots, replace)
graph combine figure6_plots f6_legend, cols(1) name(figure6, replace)
graph export "results/figure6_CMS_multiple_changes.png", replace width(1000)				

/******************************************************************************/
* Statistics for tables

* Only changing approach to standardising measures
ktau_ing, std(2) grp(1) wt(1) output("ktau_results_part")
matrix ktau_results = ktau_results_part

* Only changing approach to grouping measures
ktau_ing, std(1) grp(2) wt(1) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Only changing weights given to measure domains
ktau_ing, std(1) grp(1) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing standardisation and grouping
ktau_ing, std(2) grp(2) wt(1) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing standardisation and weights
ktau_ing, std(2) grp(1) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing weights and grouping
ktau_ing, std(1) grp(2) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

* Changing standardisation, grouping and weights
ktau_ing, std(2) grp(2) wt(2) output("ktau_results_part")
matrix ktau_results = ktau_results \ ktau_results_part

putexcel set results/raw_results.xlsx, sheet(ktau, replace) replace
#delimit ;
putexcel 	A1=("Kendall's Tau correlation coefficient") 
			C1=("Tau") 
			E1=("Tau")
			G1=("Tau")
			I1=("Tau")
			K1=("Tau")
			M1=("Tau")
			B2=("Only changing approach to standardising measures")
			B3=("Only changing approach to grouping measures")
			B4=("Only changing weights given to measure domains")
			B5=("Changing standardisation and grouping")
			B6=("Changing standardisation and weights")
			B7=("Changing grouping and weights")
			B8=("Changing standardisation, grouping and weights")
			C2=matrix(ktau_results)
			;
#delimit cr

/* changes in places - total counts */
count if !missing(std1_grp1_wt1_rank)
local n_all = r(N)
count if !missing(std1_grp1_wt1_rank) & grp1_cat == 0
local n_0 = r(N)
count if !missing(std1_grp1_wt1_rank) & inlist(grp1_cat,1,2,3)
local n_1 = r(N)
count if !missing(std1_grp1_wt1_rank) & grp1_cat == 1
local n_1a = r(N)
count if !missing(std1_grp1_wt1_rank) & grp1_cat == 2
local n_1b = r(N)
count if !missing(std1_grp1_wt1_rank) & grp1_cat == 3
local n_1c = r(N)

putexcel set results/raw_results.xlsx, sheet(totals, replace) modify
#delimit ;
putexcel	A2=("Total hospitals with CMS Star Rating")
			C1=("All hospitals")
			E1=("No missing domains")
			G1=("Any missing domains")
			I1=("One missing domain")
			K1=("Two missing domains")
			M1=("Three or more missing domains")
			C2=("N")
			D2=("(%)")
			E2=("N")
			F2=("(%)")
			G2=("N")
			H2=("(%)")
			I2=("N")
			J2=("(%)")
			K2=("N")
			L2=("(%)")
			M2=("N")
			N2=("(%)")
			C3=`n_all'
			E3=`n_0'
			G3=`n_1'
			I3=`n_1a'
			K3=`n_1b'
			M3=`n_1c'
			;
#delimit cr

/* 4 stars */
local move 4

local std 2
local grp 1
local wt  1

tab std1_grp1_wt1_stars std`std'_grp`grp'_wt`wt'_stars
tab std1_grp1_wt1_stars std`std'_grp`grp'_wt`wt'_stars if grp1_cat == 0
tab std1_grp1_wt1_stars std`std'_grp`grp'_wt`wt'_stars if grp1_cat == 3


* Only changing approach to standardising measures
count_ing, std(2) grp(1) wt(1) move(`move') output(count_results_1)
matrix count_results = count_results_1

* Only changing approach to grouping measures
count_ing, std(1) grp(2) wt(1) move(`move') output(count_results_1)
matrix count_results = count_results \ count_results_1

* Only changing weights given to measure domains
count_ing, std(1) grp(1) wt(2) move(`move') output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing standardisation and grouping
count_ing, std(2) grp(2) wt(1) move(`move') output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing standardisation and weights
count_ing, std(2) grp(1) wt(2) move(`move') output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing weights and grouping
count_ing, std(1) grp(2) wt(2) move(`move') output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing standardisation, grouping and weights
count_ing, std(2) grp(2) wt(2) move(`move') output(count_results_1)
matrix count_results = count_results \ count_results_1

matrix list count_results


putexcel set results/raw_results.xlsx, sheet(moves, replace) modify
#delimit ;
putexcel	A2=("Hospitals going from 5 stars to 1 stars, or vice versa")
			B3=("Only changing approach to standardising measures")
			B4=("Only changing approach to grouping measures")
			B5=("Only changing weights given to measure domains")
			B6=("Changing standardisation and grouping")
			B7=("Changing standardisation and weights")
			B8=("Changing grouping and weights")
			B9=("Changing standardisation, grouping and weights")
			C1=("All hospitals")
			E1=("No missing domains")
			G1=("Any missing domains")
			I1=("One missing domain")
			K1=("Two missing domains")
			M1=("Three or more missing domains")
			C2=("N")
			D2=("(%)")
			E2=("N")
			F2=("(%)")
			G2=("N")
			H2=("(%)")
			I2=("N")
			J2=("(%)")
			K2=("N")
			L2=("(%)")
			M2=("N")
			N2=("(%)")
			C3=matrix(count_results)
			;
#delimit cr

* Only changing approach to standardising measures
count2_ing, std(2) grp(1) wt(1) output(count_results_1)
matrix count_results = count_results_1

* Only changing approach to grouping measures
count2_ing, std(1) grp(2) wt(1) output(count_results_1)
matrix count_results = count_results \ count_results_1

* Only changing weights given to measure domains
count2_ing, std(1) grp(1) wt(2)output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing standardisation and grouping
count2_ing, std(2) grp(2) wt(1) output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing standardisation and weights
count2_ing, std(2) grp(1) wt(2) output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing weights and grouping
count2_ing, std(1) grp(2) wt(2) output(count_results_1)
matrix count_results = count_results \ count_results_1

* Changing standardisation, grouping and weights
count2_ing, std(2) grp(2) wt(2) output(count_results_1)
matrix count_results = count_results \ count_results_1

matrix list count_results


putexcel set results/raw_results.xlsx, sheet(moves2, replace) modify
#delimit ;
putexcel	A2=("Hospitals going from 4/5 stars to 1/2 stars, or vice versa")
			B3=("Only changing approach to standardising measures")
			B4=("Only changing approach to grouping measures")
			B5=("Only changing weights given to measure domains")
			B6=("Changing standardisation and grouping")
			B7=("Changing standardisation and weights")
			B8=("Changing grouping and weights")
			B9=("Changing standardisation, grouping and weights")
			C1=("All hospitals")
			E1=("No missing domains")
			G1=("Any missing domains")
			I1=("One missing domain")
			K1=("Two missing domains")
			M1=("Three or more missing domains")
			C2=("N")
			D2=("(%)")
			E2=("N")
			F2=("(%)")
			G2=("N")
			H2=("(%)")
			I2=("N")
			J2=("(%)")
			K2=("N")
			L2=("(%)")
			M2=("N")
			N2=("(%)")
			C3=matrix(count_results)
			;
#delimit cr

