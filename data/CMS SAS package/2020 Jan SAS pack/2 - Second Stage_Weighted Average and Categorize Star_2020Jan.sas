*Note: it takes less than 5 minutes to run this program;
****************************************************************;
* SECOND STAGE - 											   *;
* DERIVING SUMMARY SCORE AND STAR RATING                       *;
*														       *;
* SAS 9.4 WIN			   									   *;
*                        									   *;
* YALE CORE 								                   *;
* email: cmsstarratings@lantanagroup.com                       *;
****************************************************************;

/*This program is used to derive the hospital stars. The summary
  score of each hospital is calulated based on the hospital's
  group(domain) scores which are output from program 1. Then
  the K-means approach is used to derive the hospital star ratings.
*/

%LET PATH1=H:\Star Rating\2020\2020 Jan SAS pack\input data ; /* For 2020 Jan released data, MUST BE CHANGED */
%LET PATH2=H:\Star Rating\2020\2020 Jan SAS pack\derived data ; /* For derived data sets, MUST BE CHANGED */
%LET PATH3=H:\Star Rating\2020\2020 Jan SAS pack ; /* For SAS macros , MUST BE CHANGED  */

LIBNAME HC "&PATH1";
LIBNAME R "&PATH2";

%INCLUDE "&path3.\Star_Macros.sas";/*calling macros*/

%LET year = 2020;
%LET quarter = Jan;

%LET MEASURE_MEAN=R.measure_average_stddev_&year.&quarter; /*mean and standard deviation of the original measure scores*/
%LET MEASURE_ANALYSIS=R.Std_data_&year.&quarter._analysis; /* derived final analysis file */
%LET RESULTS=R.Star_&year.&quarter; /* derive hospital-level Star file */
%LET NATIONAL_MEAN=R.national_average_&year.&quarter; /* derived national average group and summary scores results */

proc printto new log="&path2.\program2.log"  print="&path2.\program2.lst";run;*output LOG and LST files;

/* put all group scores into one file */
data all; 
merge  R.Pred_outcome_mortality(keep=provider_id pred StdErrPred lower upper 
                     rename =(pred=Std_Outcomes_Mortality_score StdErrPred=SE_Outcomes_Mortality_score 
                              lower=lower_Outcomes_Mortality_score upper=upper_Outcomes_Mortality_score))
       R.Pred_outcome_readmission(keep = provider_id pred StdErrPred lower upper 
                     rename = (pred=Std_Outcomes_Readmission_score StdErrPred=SE_Outcomes_Readmission_score 
                               lower=lower_Outcomes_Readmission_score upper=upper_Outcomes_Readmission_score))
       R.Pred_outcome_safety(keep=provider_id pred StdErrPred lower upper 
                     rename =(pred=Std_Outcomes_Safety_score StdErrPred=SE_Outcomes_Safety_score 
                              lower=lower_Outcomes_Safety_score upper=upper_Outcomes_Safety_score))
	   R.Pred_ptexp(keep = provider_id pred StdErrPred lower upper 
                     rename = (pred=Std_PatientExp_score StdErrPred=SE_PatientExp_score
                               lower=lower_PtExp_score upper=upper_PtExp_score))
       R.Pred_efficiency(keep=provider_id pred StdErrPred lower upper 
                     rename =(pred=Std_Efficiency_score StdErrPred=SE_Efficiency_score 
                              lower=lower_Efficiency_score upper=upper_Efficiency_score))
       R.Pred_process_eff(keep=provider_id pred StdErrPred lower upper 
                     rename =(pred=Std_Process_Effect_score StdErrPred=SE_Process_Effect_score 
                              lower=lower_Process_Effect_score upper=upper_Process_Effect_score))
       R.Pred_process_time(keep=provider_id pred StdErrPred lower upper 
                     rename =(pred=Std_Process_Time_score StdErrPred=SE_Process_Time_score 
                              lower=lower_Process_Time_score upper=upper_Process_Time_score));
by provider_id;
run;



*********************************************************
*  CALCULATING SUMMARY SCORES BASED ON WEIGHTED AVERAGE *
* 1) fixed standard weights from CMS				    *
* 2) redistribute weights when there is missing group   *
*													    *
*  CALCULTAING 95% CI of SUMMARY SCORES         	    *
*********************************************************;

data summary_score (drop=I1-I7 w1-w7 grp_score1-grp_score7 k sum_weight_ave1-sum_weight_ave7 var1-var7);
set all;

array std_weight(7) std_weight_PatientExperience std_weight_Readmission std_weight_Mortality std_weight_safety 
                    std_weight_Efficiency std_weight_Process_Timeliness std_weight_Process_Effective;

array w(7) w1-w7;

array weight(7) weight_PatientExperience weight_Outcomes_Readmission weight_Outcomes_Mortality 
                weight_Outcomes_Safety weight_Efficiency weight_Process_time weight_Process_Effect;

array pred(7)   Std_PatientExp_score Std_Outcomes_Readmission_score Std_Outcomes_Mortality_score 
                Std_Outcomes_Safety_score Std_Efficiency_score Std_Process_Time_score Std_Process_Effect_score;

array grp_score(7) grp_score1-grp_score7;
	
array I(7) I1 I2 I3 I4 I5 I6 I7;

array sum_weight_ave(7) sum_weight_ave1 sum_weight_ave2 sum_weight_ave3 sum_weight_ave4 
                        sum_weight_ave5 sum_weight_ave6 sum_weight_ave7;

array var(7) var1-var7;

array StdErrPred(7) SE_PatientExp_score SE_Outcomes_Readmission_score SE_Outcomes_Mortality_score 
                    SE_Outcomes_Safety_score SE_Efficiency_score SE_Process_Time_score SE_Process_Effect_score;

/* fixed standard weights from CMS */;
std_weight_PatientExperience = 0.22;
std_weight_Readmission = 0.22;
std_weight_Mortality = 0.22;
std_weight_safety = 0.22;
std_weight_Efficiency = 0.04;
std_weight_Process_Timeliness = 0.04;
std_weight_Process_Effective  = 0.04;

/* create indicator for missing group*/
do k=1 to 7;
w[k]= std_weight[k];
grp_score[k]=pred[k];

if grp_score[k] =. then I[k]=1;
   else I[k]=0;
end;

/* Redistribute weights when there is missing group. For details, please refer to technical report. */
/*For example, the group of efficient use of imaging is missing, the weight for mortality groups is changed 
from 22/100 to 22/96, the weight for effectiveness of care group is changed from 4/100 to 4/96.*/
do k=1 to 7;
if grp_score[k] =. then weight[k]=.;
   else weight[k]=W[k]/(1-I1*W1-I2*W2-I3*W3-I4*W4-I5*W5-I6*W6-I7*W7);
end;

do k=1 to 7;
if weight[k] =. then sum_weight_ave[k]=0;
	else sum_weight_ave[k]=weight[k]*pred[k];

if weight[k] =. then var[k]=0;
else var[k]=weight[k]*weight[k]*StdErrPred[k]*StdErrPred[k];

end;

/* summary scores */
summary_score = sum (of sum_weight_ave1-sum_weight_ave7);


/* variation of summary scores */
var_summary_score = sum (of var1-var7);
std_summary_score = sqrt(var_summary_score);

/* 95% CI of weighted average */

Lower_summary_score = summary_score - 1.96*std_summary_score;
Upper_summary_score = summary_score + 1.96*std_summary_score;
run;


**********************************************************************************************************
** reporting criteria - minimim 3 measures/per group and 3 groups with at least outcome to receive a Star*
**********************************************************************************************************;

*exclude measures with significant negative loadings if there are any;
data measures;
length measure_in_std $204 parameter $20 ;
set R.Est_outcome_mortality R.Est_outcome_readmission R.Est_outcome_safety R.Est_ptexp 
	R.Est_efficiency R.Est_process_eff R.Est_process_time;
run;

PROC SQL;
select measure_in_std
into: measure_in_std separated by '' notrim
from measures;
QUIT;

data MEASURE_ANALYSIS2;
set &MEASURE_ANALYSIS;
keep provider_ID &measure_in_std;
RUN;

/* create reporting indicator, number of measure per measure group, total number of measure group with
  >=3 measures, and total number of outcome measure group with >= 3 measures*/
%report(MEASURE_ANALYSIS2, report_indicator, &measure_OM_cnt, &measure_OS_cnt, &measure_OR_cnt, &measure_PtExp_cnt, 
	    &measure_Ef_cnt, &measure_ProcessT_cnt, &measure_ProcessE_cnt);

/* add reporting indicator and number of measure per measure group to Star output file */
proc sort data=summary_score out=summary_score1;
by provider_id;
run;

proc sort data=report_indicator;
by provider_id;
run;

data summary_score21;
merge summary_score1 report_indicator;
by provider_id;

drop SE_PatientExp_score SE_Outcomes_Readmission_score SE_Outcomes_Mortality_score SE_Outcomes_Safety_score SE_Efficiency_score
     SE_Process_Time_score SE_Process_Effect_score std_weight_Mortality std_weight_Readmission std_weight_safety
     std_weight_PatientExperience std_weight_Efficiency std_weight_Process_Timeliness std_weight_Process_Effective var_summary_score
	 std_summary_score;
run;
data summary_score2;
set summary_score21;
if report_indicator=1;
run;


***********************************
* Generate Star Rarting by K-Means *
***********************************;
*Step1. using quintile medians as intial seeds,  run K-means to complete convergence;
proc univariate data=Summary_score2 noprint;
var summary_score;
output out=s1 pctlpre=P pctlpts=20 to 100 by 20;
run;

data s2(keep=provider_ID summary_score grp P20 P40 P60 P80);
set summary_score2;
if _n_=1 then set s1;

if .<summary_score<=P20 then grp=1;
if P20<summary_score<=P40 then grp=2;
if P40<summary_score<=P60 then grp=3;
if P60<summary_score<=P80 then grp=4;
if P80<summary_score then grp=5;
run;

proc means data=s2 median;var summary_score;class grp;
ods output summary=s3;
run;

data s33;set s3;summary_score=summary_score_median;run;

/* k-means*/
proc fastclus data=Summary_score2 maxc=5 converge=0 maxiter=1000 seed=s33;************;
var	summary_score;

ods output ClusterCenters=seeds2;
ods output  ConvergenceStatus=cstatus;
run;
proc print data=cstatus;title "QA: FASTCLUS convergence"; run;

*Step2. using results from Step 1 as initial seeds,  run K-means to complete convergence with 'strict=1' added 
to avoid potential outliers' effect on clustering;
proc fastclus data=Summary_score2 maxc=5 out=clusters converge=0 maxiter=1000 seed=seeds2 strict=1;************;
var	summary_score;

ods output ClusterCenters=Cluster_mean;
ods output  ConvergenceStatus=cstatus2;
run;
proc print data=cstatus2;title "QA: FASTCLUS convergence"; run;

/* order clusters based on mean of summary scores */
proc sort data=Cluster_mean out=cluster_sort (rename = (summary_score=mean_summary_score_star));
by summary_score;
run;

/* assign Star based on ordered mean of summary scores */
data cluster_sort (drop=cluster);
set cluster_sort;
star=_n_;
cluster_2=input(cluster,4.);
run;

proc sort data=Cluster_sort(rename=(cluster_2=cluster));
by cluster;
run;

data Clusters2;set Clusters;
cluster=abs(cluster);
run;

proc sort data=Clusters2;
by cluster;
run;

data RESULTS (drop=cluster distance mean_summary_score_star);
merge Clusters2  Cluster_sort;
by cluster;
run;

proc sort data=RESULTS;by provider_ID;run;
data &RESULTS;
merge Summary_score21 RESULTS(keep=provider_ID Star);by provider_ID;
run;



******************************
** Measure Group Categories **
******************************;

%group_3cat(indata=r.Pred_efficiency, out=efficiency);
%group_3cat(indata=r.Pred_outcome_mortality, out=outcome_mortality);
%group_3cat(indata=r.Pred_outcome_readmission, out=outcome_readmission);
%group_3cat(indata=r.Pred_outcome_safety, out=outcome_safety);
%group_3cat(indata=r.Pred_process_eff, out=process_eff);
%group_3cat(indata=r.Pred_process_time, out=process_time);
%group_3cat(indata=r.Pred_ptexp, out=ptexp);

data &RESULTS;
merge &RESULTS
	  outcome_mortality (keep=provider_id ori_cat rename = (ori_cat = Out_Mrt_Grp_Score_Cat))
      outcome_readmission (keep=provider_id ori_cat rename = (ori_cat = Out_Readm_grp_Score_Cat))
      outcome_safety (keep=provider_id ori_cat rename = (ori_cat = Out_Sft_Grp_Score_Cat))
      ptexp (keep=provider_id ori_cat rename = (ori_cat = Pt_Exp_Grp_Score_Cat))
      efficiency (keep=provider_id ori_cat rename = (ori_cat = Eff_Imaging_Grp_Score_Cat))
      process_eff (keep=provider_id ori_cat rename = (ori_cat = Prc_Effect_of_Care_Grp_Score_Cat))
      process_time (keep=provider_id ori_cat rename = (ori_cat = Prc_Time_of_Care_Grp_Score_Cat));
by provider_id;

      if Pt_Exp_Grp_Score_Cat = '' then Pt_Exp_Grp_Score_Cat = 'missing';
      if Out_Readm_grp_Score_Cat = '' then Out_Readm_grp_Score_Cat = 'missing';
      if Out_Mrt_Grp_Score_Cat = '' then Out_Mrt_Grp_Score_Cat = 'missing';
      if Out_Sft_Grp_Score_Cat  = '' then Out_Sft_Grp_Score_Cat = 'missing';
      if Eff_Imaging_Grp_Score_Cat = '' then Eff_Imaging_Grp_Score_Cat = 'missing';
      if Prc_Effect_of_Care_Grp_Score_Cat = '' then Prc_Effect_of_Care_Grp_Score_Cat = 'missing';
      if Prc_Time_of_Care_Grp_Score_Cat = '' then Prc_Time_of_Care_Grp_Score_Cat = 'missing';
run;

/* OUTPUTS OF 'Star_2020Jan' ARE GENERATED*/
/* label variables, output hospital stars */
data &RESULTS;
	set &RESULTS;

		label provider_id ="Provider ID";
		label summary_score = "Hospital Summary Score";
		label star = "Star Rating";
		label Total_measure_group_cnt = "Number of Measure Groups with >=3 Measuers";
		label Outcomes_measure_group_cnt = "Number of Outcomes Measure Groups with >=3 Measuers";
		label Outcomes_Mortality_cnt = "Number of Measures in Outcomes-Mortality Group";
		label Outcomes_Readmission_cnt = "Number of Measures in Outcomes-Readmission Group";
		label Outcomes_Safety_cnt = "Number of Measures in Outcomes-Safety Group";
		label Patient_Experience_cnt = "Number of Measures in Patient Experience Group";
        label Efficiency_cnt = "Number of Measures in Efficiency Group";
		label Process_Timeliness_cnt = "Number of Measures in Process-Timeliness Group";
		label Process_Effectiveness_cnt = "Number of Measures in Process-Effectiveness Group";
		label Std_PatientExp_score = "Standardized Patient Experience Group Score"; 
		label Std_Outcomes_Readmission_score = "Standardized Outcomes-Readmission Group Score";
		label Std_Outcomes_Mortality_score = "Standardized Outcomes-Mortality Group Score";
		label Std_Outcomes_Safety_score = "Standardized Outcomes-Safety Group Score";
		label Std_Efficiency_score = "Standardized Efficiency Group Score"; 
		label Std_Process_Time_score = "Standardized Process-Timeliness Group Score";
		label Std_Process_Effect_score = "Standardized Process-Effectiveness Group Score"; 
		label weight_Outcomes_Mortality = "Outcomes-Mortality Group Weight"; 

		label lower_PtExp_score = "Lower bound - Patient Experience Group Score"; 
		label lower_Outcomes_Readmission_score = "Lower bound - Outcomes-Readmission Group Score";
		label lower_Outcomes_Mortality_score = "Lower bound - Outcomes-Mortality Group Score";
		label lower_Outcomes_Safety_score = "Lower bound - Outcomes-Safety Group Score";
		label lower_Efficiency_score = "Lower bound - Efficiency Group Score"; 
		label lower_Process_Time_score = "Lower bound - Process-Timeliness Group Score";
		label lower_Process_Effect_score = "Lower bound - Process-Effectiveness Group Score"; 

		label upper_PtExp_score = "Upper bound - Patient Experience Group Score"; 
		label upper_Outcomes_Readmission_score = "Upper bound - Outcomes-Readmission Group Score";
		label upper_Outcomes_Mortality_score = "Upper bound - Outcomes-Mortality Group Score";
		label upper_Outcomes_Safety_score = "Upper bound - Outcomes-Safety Group Score";
		label upper_Efficiency_score = "Upper bound - Efficiency Group Score"; 
		label upper_Process_Time_score = "Upper bound - Process-Timeliness Group Score";
		label upper_Process_Effect_score = "Upper bound - Process-Effectiveness Group Score"; 

		label weight_Outcomes_Readmission = "Outcomes-Readmission Group Weight"; 
		label weight_Outcomes_Safety = "Outcomes-Safety Group Weight"; 
		label weight_PatientExperience = "Patient Experience Group Weight"; 
		label weight_Efficiency = "Efficiency Group Weight"; 
		label weight_Process_Time = "Process-Timeliness Group Weight"; 
		label weight_Process_Effect = "Process-Effectiveness Group Weight";
		label Pt_Exp_Grp_Score_Cat = "Patient Experience Group Categories";
		label Out_Readm_grp_Score_Cat = "Outcomes-Readmission Group Categories";
		label Out_Mrt_Grp_Score_Cat = "Outcomes-Mortality Group Categories";
		label Out_Sft_Grp_Score_Cat = "Outcomes-Safety Group Categories";
		label Eff_Imaging_Grp_Score_Cat = "Efficiency Imaging Group Categories";
		label Prc_Effect_of_Care_Grp_Score_Cat = "Process-Effectiveness Group Categories";
		label Prc_Time_of_Care_Grp_Score_Cat = "Process-Timeliness Group Categories";
		label report_indicator = "Indicator for reporting Star Rating";
run;

**************************************
** National Average of Group Scores **
**************************************;
%nation_avg0(indata=&RESULTS, out=summary_avg, var=Summary_Score_Nat);

%nation_avg(indata=r.Pred_outcome_mortality, out=outcome_mortality_avg, var=Out_Mrt_Grp_Score_Nat);
%nation_avg(indata=r.Pred_outcome_safety, out=outcome_safety_avg, var=Out_Sft_Grp_Score_Nat);
%nation_avg(indata=r.Pred_outcome_readmission, out=outcome_readmission_avg, var=Out_Readm_grp_Score_Nat);
%nation_avg(indata=r.Pred_ptexp, out=ptexp_avg, var=Pt_Exp_Grp_Score_Nat);
%nation_avg(indata=r.Pred_process_eff, out=process_eff_avg, var=Prc_Eff_of_Care_Grp_Score_Nat);
%nation_avg(indata=r.Pred_process_time, out=process_time_avg, var=Prc_Time_of_Care_Grp_Score_Nat);
%nation_avg(indata=r.Pred_efficiency, out=efficiency_avg, var=Eff_Imaging_Grp_Score_Nat);

data &NATIONAL_MEAN;
	merge summary_avg outcome_mortality_avg outcome_safety_avg outcome_readmission_avg ptexp_avg
process_eff_avg process_time_avg efficiency_avg;
run;

/* OUTPUTS OF 'national_average_2020Jan' ARE GENERATED*/ 
/* label variables for the national average file, output national average group and summary scores results*/
data &NATIONAL_MEAN;
	set &NATIONAL_MEAN;

		label Summary_Score_Nat = "National Mean of Summary Score";
		label Pt_Exp_Grp_Score_Nat = "National Mean of Patient Experience Group Score";
		label Out_Readm_grp_Score_Nat = "National Mean of Outcomes-Readmission Group Score";
		label Out_Mrt_Grp_Score_Nat = "National Mean of Outcomes-Mortality Group Score";
		label Out_Sft_Grp_Score_Nat = "National Mean of Outcomes-Safety Group Score";
		label Eff_Imaging_Grp_Score_Nat = "National Mean of Efficiency Imaging Group Score";
		label Prc_Eff_of_Care_Grp_Score_Nat = "National Mean of Process-Effectiveness Group Score";
		label Prc_Time_of_Care_Grp_Score_Nat = "National Mean of Process-Timeliness Group Score";
run;


proc printto;run;*end of LOG/LST file output;
