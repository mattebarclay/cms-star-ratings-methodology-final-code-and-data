/******************************************************************************/
/* 	Monte Carlo simulation to examine the sensitivty of the CMS Star Ratings
	to methodological choices in even more detail.
	
	2020-11-25
	Version 1.0
*/

/******************************************************************************/
* Set random number seed
set seed 4334

/******************************************************************************/
* Change directory
cd "C:\Users\matte\Documents\cms_star_ratings\cms-star-ratings-methodology-final-code-and-data"


/******************************************************************************/
* Number of simulations to do
local sims 10000

/******************************************************************************/
/* Load simulation program */
do mc_weights_cms.do

/******************************************************************************/
/* Program for assigning stars*/
cap program drop assign_stars
program define assign_stars
	syntax varname , gen(string)
	
	tempvar s2 one tempstar
	
	qui {
		gen `gen' = .

		cluster kmeans `varlist', k(5) gen(`tempstar')
		
		gen `s2' = .
		forval i = 1/5 {
			summ `varlist' if `tempstar' == `i', meanonly
			replace `s2' = r(mean) if `tempstar' == `i'
		}
		sort sim `s2' `varlist' provider_id
		by sim `s2': gen `one' = _n == 1 if !missing(`s2')
		replace `one' = sum(`one') if !missing(`s2')
		replace `gen' = `one'
		drop `s2' `one' `tempstar'

		sort provider_id
		
	}
	
end

/******************************************************************************/
/* Plot of weights */

clear
local obs 1000
set obs `obs'
gen x = logit((_n-.5)/`obs')
gen dist1 = normalden(x,logit(0.22),1.5)
gen dist2 = normalden(x,logit(0.04),1.5)

*twoway line dist1 x, sort(x)

gen x_prob = invlogit(x)

#delimit ;
twoway 	(line dist1 x_prob, sort(x_prob) lcol(blue)	)  
		(line dist2 x_prob, sort(x_prob) lcol(red)	)  
		,	xlabel(0(0.2)1, format(%02.1f)) 
			ylabel(0(0.1)0.3, format(%02.1f) angle(h) tl(0))
			xline(0.22, lpattern(dash) lcol(blue%20))
			xline(0.04, lpattern(dash) lcol(red%20))
			xtitle("Measure weight")
			ytitle("Density")
			plotregion(margin(b=0))
			legend(
				subtitle("Distribution of measure weights")
				order(
					1 "Outcome domains"
					2 "Process domains"
				)
				ring(0) pos(2) cols(1)
			)
			text( 0 0.03 "0.04", place(se) size(vsmall) )
			text( 0 0.21 "0.22", place(se) size(vsmall) )
			name(om_density, replace)
		;
#delimit cr
graph export "results/figure3_cms_measure_weights.png", width(1000) replace

gen cdf1 = sum(dist1)
egen t1 = total(dist1)
replace cdf1 = cdf1/t1

gen cdf2 = sum(dist2)
egen t2 = total(dist2)
replace cdf2 = cdf2/t2

list if cdf1 >= 0.024 & cdf1 <= 0.026
list if cdf1 >= 0.974 & cdf1 <= 0.976
* x = -2.933962 // .0505 
* x = +1.446765 // .8095

list if cdf2 >= 0.02 & cdf2 <= 0.03
list if cdf2 >= 0.974 & cdf2 <= 0.976
* x = -4.291474	// 0.0115
* x = +.4368156 // 0.6075

/******************************************************************************/
/* Prepare data */
tempfile file1 file2 file3 file4
use "data\std_method1_grp_pol", clear
rename 	(mortality_m1 safety_m1 readm_m1 ptexp_m1 effic_m1 timely_m1 effec_m1) ///
		(	std0_grp0_measure1 /// 
			std0_grp0_measure2 /// 
			std0_grp0_measure3 /// 
			std0_grp0_measure4 /// 
			std0_grp0_measure5 /// 
			std0_grp0_measure6 /// 
			std0_grp0_measure7 )
save `file1'

use "data\std_method1_grp_efa", clear
rename (f1 f2 f3 f4 f5 f6) /// 
		(	std0_grp1_measure1 /// 
			std0_grp1_measure2 /// 
			std0_grp1_measure3 /// 
			std0_grp1_measure4 /// 
			std0_grp1_measure5 /// 
			std0_grp1_measure6 )
* f4 and f6 are 'process'
save `file2'

use "data\std_method2_grp_pol", clear
rename 	(mortality_m2 safety_m2 readm_m2 ptexp_m2 effic_m2 timely_m2 effec_m2) ///
		(	std1_grp0_measure1 /// 
			std1_grp0_measure2 /// 
			std1_grp0_measure3 /// 
			std1_grp0_measure4 /// 
			std1_grp0_measure5 /// 
			std1_grp0_measure6 /// 
			std1_grp0_measure7 )
save `file3' 

use "data\std_method2_grp_efa", clear
rename (g1 g2 g3 g4 g5 g6) /// 
		(	std1_grp1_measure1 /// 
			std1_grp1_measure2 /// 
			std1_grp1_measure3 /// 
			std1_grp1_measure4 /// 
			std1_grp1_measure5 /// 
			std1_grp1_measure6 )
* g4 and g6 are 'process'
save `file4'

use `file1', clear
merge 1:1 provider_id using `file2', assert(3) nogenerate
merge 1:1 provider_id using `file3', assert(3) nogenerate
merge 1:1 provider_id using `file4', assert(3) nogenerate

egen nm = rownonmiss(std0_grp0_*)
keep if nm >= 3 & (!missing(std0_grp0_measure1) | !missing(std0_grp0_measure2) | !missing(std0_grp0_measure3))
drop nm

egen missing_domains = rowmiss(std0_grp0_*)
label var missing_domains "Number of missing domains"

compress
label data "Data for Monte Carlo simulations"

save data/final_cms_simulation_basedata, replace

/*
/******************************************************************************/
/* CMS simulation - WEIGHTS ONLY */

use data/final_cms_simulation_basedata, clear

mc_cms, std_sim(0) grp_sim(0) sims(`sims') save(cms_weights_std0_grp0_sims)

tempfile file1 file2
save `file1', replace

* WARNING - this loop is extremely slow
forval sim = 1/`sims' {
	use if sim == `sim' using `file1'
	
	assign_stars wtd_summary_score, gen(stars)
	
	if `sim' == 1 {
		save `file2', replace
	}
	else {
		append using `file2'
		save `file2', replace
	}
}

egen sd_between = sd(wtd_summary_rank), by(sim)
gen var_between = sd_between^2

#delimit ;
collapse 	(mean) 	mean		= wtd_summary_score 
			(min)	min  		= wtd_summary_score 
			(p25)  	lb 			= wtd_summary_score 
			(p75) 	ub 			= wtd_summary_score
			(max) 	max  		= wtd_summary_score
			(sd)  	sd_within 	= wtd_summary_rank
			(mean) 	var_between = var_between
			(mean) 	mean_rank	= wtd_summary_rank 
			(min)	min_rank	= wtd_summary_rank 
			(p25)  	lb_rank		= wtd_summary_rank 
			(p75) 	ub_rank		= wtd_summary_rank
			(max) 	max_rank	= wtd_summary_rank
			(p50) 	med_stars	= stars 
			(min)	min_stars	= stars 
			(p25)  	lb_stars	= stars 
			(p75) 	ub_stars	= stars
			(max) 	max_stars	= stars
			, 	by(provider_id)
			;
#delimit cr

save data/cms_weights_std0_grp0, replace

/******************************************************************************/
/* CMS simulation - WEIGHTS AND GROUPS */

use data/final_cms_simulation_basedata, clear

mc_cms, std_sim(0) grp_sim(1) sims(`sims') save(cms_weights_std0_grp1_sims)
	
tempfile file1 file2
save `file1', replace
/*
forval sim = 1/`sims' {
	use if sim == `sim' using `file1'
	
	assign_stars wtd_summary_score, gen(stars)
	
	if `sim' == 1 {
		save `file2', replace
	}
	else {
		append using `file2'
		save `file2', replace
	}
}
*/
egen sd_between = sd(wtd_summary_rank), by(sim)
gen var_between = sd_between^2

#delimit ;
collapse 	(mean) 	mean		= wtd_summary_score 
			(min)	min  		= wtd_summary_score 
			(p25)  	lb 			= wtd_summary_score 
			(p75) 	ub 			= wtd_summary_score
			(max) 	max  		= wtd_summary_score
			(sd)  	sd_within 	= wtd_summary_rank
			(mean) 	var_between = var_between
			(mean) 	mean_rank	= wtd_summary_rank 
			(min)	min_rank	= wtd_summary_rank 
			(p25)  	lb_rank		= wtd_summary_rank 
			(p75) 	ub_rank		= wtd_summary_rank
			(max) 	max_rank	= wtd_summary_rank
			(p50) 	med_stars	= stars 
			(min)	min_stars	= stars 
			(p25)  	lb_stars	= stars 
			(p75) 	ub_stars	= stars
			(max) 	max_stars	= stars
			, 	by(provider_id)
			;
#delimit cr

save data/cms_weights_std0_grp1, replace

/******************************************************************************/
/* CMS simulation - WEIGHTS AND STANDARDISATION */

use data/final_cms_simulation_basedata, clear

mc_cms, std_sim(1) grp_sim(0) sims(`sims') save(cms_weights_std1_grp0_sims)
	
tempfile file1 file2
save `file1', replace
/*
forval sim = 1/`sims' {
	use if sim == `sim' using `file1'
	
	assign_stars wtd_summary_score, gen(stars)
	
	if `sim' == 1 {
		save `file2', replace
	}
	else {
		append using `file2'
		save `file2', replace
	}
}
*/
egen sd_between = sd(wtd_summary_rank), by(sim)
gen var_between = sd_between^2

#delimit ;
collapse 	(mean) 	mean		= wtd_summary_score 
			(min)	min  		= wtd_summary_score 
			(p25)  	lb 			= wtd_summary_score 
			(p75) 	ub 			= wtd_summary_score
			(max) 	max  		= wtd_summary_score
			(sd)  	sd_within 	= wtd_summary_rank
			(mean) 	var_between = var_between
			(mean) 	mean_rank	= wtd_summary_rank 
			(min)	min_rank	= wtd_summary_rank 
			(p25)  	lb_rank		= wtd_summary_rank 
			(p75) 	ub_rank		= wtd_summary_rank
			(max) 	max_rank	= wtd_summary_rank
			(p50) 	med_stars	= stars 
			(min)	min_stars	= stars 
			(p25)  	lb_stars	= stars 
			(p75) 	ub_stars	= stars
			(max) 	max_stars	= stars
			, 	by(provider_id)
			;
#delimit cr

save data/cms_weights_std1_grp0, replace
*/
/******************************************************************************/
/* CMS simulation - WEIGHTS AND GROUPS AND STANDARDISATION */

use data/final_cms_simulation_basedata, clear

mc_cms, std_sim(1) grp_sim(1) sims(`sims') save(cms_weights_std1_grp1_sims)
	
tempfile file1 file2
save `file1', replace
/*
forval sim = 1/`sims' {
	use if sim == `sim' using `file1'
	
	assign_stars wtd_summary_score, gen(stars)
	
	if `sim' == 1 {
		save `file2', replace
	}
	else {
		append using `file2'
		save `file2', replace
	}
}
*/
egen sd_between = sd(wtd_summary_rank), by(sim)
gen var_between = sd_between^2

#delimit ;
collapse 	(mean) 	mean		= wtd_summary_score 
			(min)	min  		= wtd_summary_score 
			(p25)  	lb 			= wtd_summary_score 
			(p75) 	ub 			= wtd_summary_score
			(max) 	max  		= wtd_summary_score
			(sd)  	sd_within 	= wtd_summary_rank
			(mean) 	var_between = var_between
			(mean) 	mean_rank	= wtd_summary_rank 
			(min)	min_rank	= wtd_summary_rank 
			(p25)  	lb_rank		= wtd_summary_rank 
			(p75) 	ub_rank		= wtd_summary_rank
			(max) 	max_rank	= wtd_summary_rank
			/*(p50) 	med_stars	= stars 
			(min)	min_stars	= stars 
			(p25)  	lb_stars	= stars 
			(p75) 	ub_stars	= stars
			(max) 	max_stars	= stars*/
			, 	by(provider_id) fast
			;
#delimit cr

save data/cms_weights_std1_grp1, replace
