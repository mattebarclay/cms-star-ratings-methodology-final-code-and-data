*Note: it takes about 40 hours to run this program;
****************************************************************;
* FIRST STAGE - 											   *;
* DERIVING GROUP SCORE FOR EACH MEASURE TYPE GROUP 			   *;
*														       *;
* SAS 9.4 WIN			   									   *;
*                        									   *;
* YALE CORE 								                   *;
* email: cmsstarratings@lantanagroup.com                       *;
****************************************************************;

/*This program is used to calulate the 7 groups(domains) scores for 
  each hospital with 7 latent variable models(LVM). 
  The 7 groups(domains) are:
  (1)Mortality  
  (2)Safety of Care
  (3)Readmission 
  (4)Patient Experience 
  (5)Effectiveness of Care
  (6)Timeliness of Care
  (7)Efficient Use of Imaging
  The 7 groups(domains) scores of each hospitals will be used in program 2 
  to derive the hospital star ratings. 
*/

%LET PATH1=H:\Star Rating\2020\2020 Jan SAS pack\input data ; /* For 2020 Jan released data, MUST BE CHANGED */
%LET PATH2=H:\Star Rating\2020\2020 Jan SAS pack\derived data ; /* For derived data sets, MUST BE CHANGED */
%LET PATH3=H:\Star Rating\2020\2020 Jan SAS pack ; /* For SAS macros , MUST BE CHANGED  */

LIBNAME HC "&PATH1";
LIBNAME R "&PATH2";

%INCLUDE "&path3.\Star_Macros.sas";/*calling macros*/

%LET year = 2020;
%LET quarter = Jan;

%LET MEASURE_MEAN=R.measure_average_stddev_&year.&quarter; /*mean and standard deviation of the original measure scores*/
%LET MEASURE_ANALYSIS=R.Std_data_&year.&quarter._analysis; /* derived final analysis file */
%LET RESULTS=R.Star_&year.&quarter; /* derive hospital-level Star file */
%LET NATIONAL_MEAN=R.national_average_&year.&quarter; /* derived national average group and summary scores results */

proc printto new log="&path2.\program1.log" print="&path2.\program1.lst";run;*output LOG and LST files;

/* denominator for measuers in patient experience measure group*/
%LET den_PtExp = H_NUMB_COMP H_RESP_RATE_P; 

******************************************
* Outcomes - Mortality 					 *
******************************************;
*option mprint;
/* count number of measures in Outcome Mortality Group */
PROC SQL;
select count(measure_in_name)
into: measure_OM_cnt /*number of measures in this domain*/
from Outcomes_mortality;/*Outcomes_mortality is generated from the SAS program '0 - Data and Measure Standardization_2020Jan'*/
QUIT;

/* Sub dataset for Outcome Mortality Group */;
/*OM is used to define the data name for mortality Group; 
&measure_OM is the measures in Mortality Group;
&den_OM is the measure list for denominators in Mortality Group; 
&measure_OM_cnt is the number of measures in this Group;*/
%sub_data(&MEASURE_ANALYSIS, OM, &measure_OM,  &den_OM, &measure_OM_cnt);

/* calculate the weights by denominator for accounting for sampling variation*/
%denominator(OM, &den_OM, &measure_OM, &measure_OM_cnt);

/* calcuate group score */
/*output LVM model parameters in R.Est_outcome_mortality, output group score in R.Pred_outcome_mortality */
%LVM(Outcomes_mortality, OM, &measure_OM, &measure_OM_cnt, R.Est_outcome_mortality, R.Pred_outcome_mortality);


***************************************
* Outcomes - Safety of Care			  *
***************************************;

/* count number of measures in Outcome Safety Group */
PROC SQL;
select count(measure_in_name)
into: measure_OS_cnt
from Outcomes_safety;/*Outcomes_safety is generated from the SAS program '0 - Data and Measure Standardization_2020Jan'*/
QUIT;

/* Sub dataset for Outcome Safety Group */;
/*OS is used to define the data name for Safety Group; 
&measure_OS is the measures in Safety Group;
&den_OS is the measure list for denominators in Safety Group; 
&measure_OS_cnt is the number of measures in Safety Group;*/
%sub_data(&MEASURE_ANALYSIS, OS, &measure_OS, &den_OS, &measure_OS_cnt);

/* calculate the weights by denominator for accounting for sampling variation*/
/*For Safety, no weight in LVM. So we set all weights as 1 here*/
%denominator(OS, &den_OS, &measure_OS, &measure_OS_cnt);


/* calcuate group score */
/*output LVM model parameters in R.Est_outcome_safety, output group score in R.Pred_outcome_safety */
%LVM(outcomes_safety, OS, &measure_OS, &measure_OS_cnt, R.Est_outcome_safety, R.Pred_outcome_safety);


********************************************
* Outcomes - Readmission 				   *
********************************************;

/* count number of measures in Outcome Readmission Group */
PROC SQL;
select count(measure_in_name)
into: measure_OR_cnt
from Outcomes_readmission;/*Outcomes_readmission is generated from the SAS program '0 - Data and Measure Standardization_2020Jan'*/
QUIT;

/* Sub dataset for Outcome Readmission Group */;
/*OR is used to define the data name for Readmission Group; 
&measure_OR is the measures in Readmission Group;
&den_OR is the measure list for denominators in Readmission Group; 
&measure_OR_cnt is the number of measures in Readmission Group;*/
%sub_data(&MEASURE_ANALYSIS, OR, &measure_OR, &den_OR, &measure_OR_cnt);

/* calculate the weights by denominator for accounting for sampling variation*/
%denominator(OR, &den_OR, &measure_OR, &measure_OR_cnt);

/* calcuate group score */
/*output LVM model parameters in R.Est_outcome_readmission, output group score in R.Pred_outcome_readmission */
%LVM(Outcomes_readmission, OR, &measure_OR, &measure_OR_cnt, R.Est_outcome_readmission, R.Pred_outcome_readmission);



******************************************
*  Patient Experience  					 *
******************************************;

/* count number of measures in Patient Experience Group */
PROC SQL;
select count(measure_in_name)
into: measure_PtExp_cnt
from Ptexp;/*Ptexp is generated from the SAS program '0 - Data and Measure Standardization_2020Jan'*/
QUIT;

/* Sub dataset for Patient Experience Group */;
/*PtExp is used to define the data name for Patient Experience Group; 
&measure_PtExp is the measures in Patient Experience Group;
&den_PtExp is the measure list for denominators in Patient Experience Group; 
&measure_PtExp_cnt is the number of measures in Patient Experience Group;*/
%sub_data(&MEASURE_ANALYSIS, PtExp, &measure_PtExp, &den_PtExp, &measure_PtExp_cnt);

/* calculate the weights by denominator for accounting for sampling variation*/
%denominator_PtExp(PtExp, &den_PtExp, &measure_PtExp, &measure_PtExp_cnt);

/* calcuate group score */
/*output LVM model parameters in R.Est_PtExp, output group score in R.Pred_PtExp*/
%LVM(Ptexp, PtExp, &measure_PtExp, &measure_PtExp_cnt, R.Est_PtExp,R.Pred_PtExp);


**********************************************
* Effectiveness of Care						 *
**********************************************;

/* count number of measures in Process - Effectiveness Group */
PROC SQL;
select count(measure_in_name)
into: measure_ProcessE_cnt
from ProcessE;/*ProcessE is generated from the SAS program '0 - Data and Measure Standardization_2020Jan'*/
QUIT;

/* Sub dataset for Process - Effectiveness Group */;
/*ProcessE is used to define the data name for Process - Effectiveness Group; 
&measure_ProcessE is the measures in Process - Effectiveness Group;
&den_ProcessE is the measure list for denominators in Process - Effectiveness Group; 
&measure_ProcessE_cnt is the number of measures in Process - Effectiveness Group;*/
%sub_data(&MEASURE_ANALYSIS, ProcessE, &measure_ProcessE, &den_ProcessE, &measure_ProcessE_cnt);

/* calculate the weights by denominator for accounting for sampling variation*/
%denominator(ProcessE, &den_ProcessE, &measure_ProcessE, &measure_ProcessE_cnt);

/* calcuate group score */
/*output LVM model parameters in R.Est_process_eff, output group score in R.Pred_process_eff*/
%LVM(ProcessE, ProcessE, &measure_ProcessE,  &measure_ProcessE_cnt,R.Est_process_eff, R.Pred_process_eff);


*******************************************
* Timeliness of Care					  *
*******************************************;

/* count number of measures in Process - Timeliness Group */
PROC SQL;
select count(measure_in_name)
into: measure_ProcessT_cnt
from ProcessT;/*ProcessT is generated from the SAS program '0 - Data and Measure Standardization_2020Jan'*/
QUIT;

/* Sub dataset for Process - Timeliness Group */;
/*ProcessT is used to define the data name for Process - Timeliness Group; 
&measure_ProcessT is the measures in Process - Timeliness Group;
&den_ProcessT is the measure list for denominators in Process - Timeliness Group; 
&measure_ProcessT_cnt is the number of measures in Process - Timeliness Group;*/
%sub_data(&MEASURE_ANALYSIS, ProcessT, &measure_ProcessT, &den_ProcessT, &measure_ProcessT_cnt);

/* calculate the weights by denominator for accounting for sampling variation*/
%denominator(ProcessT, &den_ProcessT, &measure_ProcessT, &measure_ProcessT_cnt);

/* calcuate group score */
/*output LVM model parameters in R.Est_Process_Time, output group score in R.Pred_Process_Time*/
%LVM(ProcessT, ProcessT, &measure_ProcessT, &measure_ProcessT_cnt,R.Est_Process_Time, R.Pred_Process_Time);



*********************************
* Efficiency Use of Imaging	    *
*********************************;

/* count number of measures in Efficiency Group */
PROC SQL;
select count(measure_in_name)
into: measure_Ef_cnt
from Efficiency;/*Efficiency is generated from the SAS program '0 - Data and Measure Standardization_2020Jan'*/
QUIT;

/* Sub dataset for Efficiency Group */;
/*EF is used to define the data name for Efficiency Group; 
&measure_EF is the measures in Efficiency Group;
&den_EF is the measure list for denominators in Efficiency Group; 
&measure_EF_cnt is the number of measures in Efficiency Group;*/
%sub_data(&MEASURE_ANALYSIS, EF, &measure_EF, &den_Ef, &measure_Ef_cnt);

/* calculate the weights by denominator for accounting for sampling variation*/
%denominator(EF, &den_EF, &measure_Ef, &measure_Ef_cnt);

/* calcuate group score */
/*output LVM model parameters in R.Est_Efficiency, output group score in R.Pred_Efficiency*/
%LVM(Efficiency, EF, &measure_Ef, &measure_Ef_cnt, R.Est_Efficiency, R.Pred_Efficiency);


proc printto;run;*end of LOG/LST file output;
/*Continue to "2 - Second Stage_Weighted Average and Categorize Star_2020Jan" SAS progam*/
