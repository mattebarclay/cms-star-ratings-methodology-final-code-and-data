This repository contains the analysis code used for Chapter 3 of the thesis.

This top-level contains the different analysis files, each run in Stata v15 for my analysis.

The folder 'data' contains the raw data downloaded from the QualityNet website, and then the various datasets produced by my analysis.

The folder 'results' contains the different (Stata) figures used in the thesis, plus some excel files of raw results used to complete tables. Other results are not in there, because they were read from Stata's results window. This is bad form but was convenient.

The folder 'simdata' may not be here, but is solely used to group four very large files containing the results of the all the individual Monte Carlo simulations.

Details of the analysis code files in order that they should be run to reproduce the results in Chapter 3 of the thesis.

The first analysis code file is starRating0_LoadDataCMS.do. This file loads in the raw data that has been converted from a SAS bdat file using STAT-transfer, and applies some initial cleaning to get the file into a format I prefer.

The second file is starRating1_Standardise.do. This file applies the current CMS and my plausible alternative standardisation approaches to the raw data.

The third file, starRating2a_Group.do, applies exploratory factor analysis to look for latent factors within the performance measures to identify possible empirical domains of quality. This file is on its own because it takes a while to run, so it is frustrating if accidentally run when it does not need to be run. Note that EFA is carried out twice, once for the current standardisation and once for the alternative approach, but these give similar results.

The fourth file, starRating2b_Group_NoEFA.do, groups the measures according to the exploratory factor analysis results, and to the current domains used in the Star Ratings.

The fifth file, starRating3_Combine.do, calculates the domain scores and the overall summary scores according to the four permutations of the CMS Star Ratings (two approaches to standardisation and two approaches to grouping measures into domains) considered so far, using two different approaches to weighting domains (the current policy-based weights and an alternate set of equal weights).

The sixth file, starRating4_Main_plots_and_results.do, produces calculates the main set of results for this analysis. This does not really do any analysis as such, it simply produces plots and tables based on the analysis done in previous files.

The seventh file, starRating5_measurement_model_impact.do, produces some results comparing the current CMS latent variable models against an approach using the mean of the observed measures within domains. These are not in the thesis chapter but are interesting.

The eighth file, starRating6_missing_domains_and_performance.do, looks at associations between the number of missing domains and hospital ranks under each of the specifications of the composite indicator.

The ninth file, starRating7_MonteCarloSimulation.do, runs the four Monte Carlo simulations showing the impact of plausible domain weights, approaches to grouping measures, and approaches to standardisation. This file takes a long time to run, on the order of weeks due to the code that assigns Star Ratings (and many hours if this code is removed).

The tenth file, starRating8_MonteCarloSimulation_results.do, produces results tables and figures based on the simulation results.

The additional file mc_weights_cms.do contains the Stata program used to make running the simulations easier.




