/* 
	Program to do Monte Carlo sim with CMS data
*/

* expand version
cap program drop mc_cms
program define mc_cms
	syntax , std_sim(int) grp_sim(int) sims(real) save(string)
	
	quietly {

		* expand it!
		expand `sims'
		sort provider_id
		by provider_id: gen sim = _n
		sort sim provider_id
		
		if `grp_sim' == 1 {
			* include grouping approach in half of sims
			by sim: gen grp_use =  rbinomial(1,0.5) if _n == 1
			replace grp_use = grp_use[_n-1] if missing(grp_use)
		}
		else {
			* just use the base approach
			gen grp_use = 0
		}
		
		if `std_sim' == 1 {
			* include standardisation approach in half of sims
			by sim: gen std_use =  rbinomial(1,0.5) if _n == 1	
			replace std_use = std_use[_n-1] if missing(std_use)
		}
		else {
			 * just use the base approach
			gen std_use = 0
		}
		
		forval i = 1/7 {
			gen mean_wt_d`i' = .
		}
		
		* gen measures to look at
		forval i = 1/7 {
			gen domain`i' = .
		}
		
		* get domain scores in consistent named variable	
		forval i = 1/6 {
			replace domain`i' = std0_grp0_measure`i' if std_use == 0 & grp_use == 0
			replace domain`i' = std1_grp0_measure`i' if std_use == 1 & grp_use == 0
			replace domain`i' = std0_grp1_measure`i' if std_use == 0 & grp_use == 1
			replace domain`i' = std1_grp1_measure`i' if std_use == 1 & grp_use == 1
		}
		replace domain7 = std0_grp0_measure7 if std_use == 0 & grp_use == 0
		replace domain7 = std1_grp0_measure7 if std_use == 1 & grp_use == 0
		* if groups are different, then no measure 7
		
		* drop irrelevant info
		drop *_measure*
		
		* if looking at alternate groupings...
		replace mean_wt_d1 = 0.22 if grp_use == 1
		replace mean_wt_d2 = 0.22 if grp_use == 1
		replace mean_wt_d3 = 0.22 if grp_use == 1
		replace mean_wt_d4 = 0.06 if grp_use == 1
		replace mean_wt_d5 = 0.22 if grp_use == 1
		replace mean_wt_d6 = 0.06 if grp_use == 1
		
		* if looking at standard groupings...
		replace mean_wt_d1 = 0.22 if grp_use == 0
		replace mean_wt_d2 = 0.22 if grp_use == 0
		replace mean_wt_d3 = 0.22 if grp_use == 0
		replace mean_wt_d4 = 0.22 if grp_use == 0
		replace mean_wt_d5 = 0.04 if grp_use == 0
		replace mean_wt_d6 = 0.04 if grp_use == 0
		replace mean_wt_d7 = 0.04 if grp_use == 0
		
		* standard deviation of weights
		local sd_wt = 1.5
		
		qui forval i = 1/7 {
			* generate random measure weights for each domain		
			by sim: gen wt_d`i' = invlogit(rnormal(logit(mean_wt_d`i'),`sd_wt')) if _n == 1
			replace wt_d`i' = wt_d`i'[_n-1] if missing(wt_d`i')
			
			* check not missing before we do any blanking
			assert !missing(wt_d`i')
			
			* blank the weight if missing domain
			replace wt_d`i' = . if missing(domain`i')
			
			* generate 'weighted performance'
			gen wtd_d`i' = wt_d`i'*domain`i'
			
		}
		* generate total weight
		egen totwt = rowtotal(wt_d*)

		* generate summary score based on total weights
		egen wtd_summary_score = rowtotal(wtd_d*)
		replace wtd_summary_score = wtd_summary_score/totwt
		
		*assign_stars wtd_summary_score, gen(stars)
		
		keep  sim provider_id wt_d* grp std /*stars*/ wtd_summary_score  
		order sim provider_id wt_d* grp std /*stars*/ wtd_summary_score 
		list sim provider_id wtd_summary_score in 1/4
		
		egen wtd_summary_rank = rank(wtd_summary_score), by(sim)
		
		label var sim "Simulation number"
		label var provider_id "Hospital ID"
		label var wt_d1 "Weight given to domain 1"
		label var wt_d2 "Weight given to domain 2"
		label var wt_d3 "Weight given to domain 3"
		label var wt_d4 "Weight given to domain 4"
		label var wt_d5 "Weight given to domain 5"
		label var wt_d6 "Weight given to domain 6"
		label var wt_d7 "Weight given to domain 7"
		label var grp   "Domain grouping used (0 = base, 1 = alternative)"
		label var std	"Standardisation used (0 = base, 1 = alternative)"
		*label var stars "Star Rating"
		label var wtd_summary_score "Summary score"
		label var wtd_summary_rank 	"Summary rank"
		
		order sim provider_id wt_d* grp std /*stars*/ wtd_summary_score wtd_summary_rank	
	
	}
	
	save simdata/`save', replace
	
end
