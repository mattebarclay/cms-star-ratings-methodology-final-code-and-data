/******************************************************************************/
/* 	Standardise
	
	This file standardises the measures used in the star ratings.
	
	First it does it using the Z-score approach of the current star ratings.
	
	Second it does this using an alternate plausible approach.
	
	Matthew Barclay
	Version 1.0
	2020-11-25
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"

/******************************************************************************/
/* Standardise measures scores - CMS approach								  */
use data/StarRatings_cleaned.dta, clear

* Standardise
pause off
foreach type in mortality readm safety ptexp effic timely effec {
	forval i = 1/11 {
		cap confirm numeric variable `type'_`i' 
		if _rc {
			* skip
		}
		else {
			desc `type'_`i'
			summ `type'_`i'
			replace `type'_`i' = ((`type'_`i')-r(mean))/r(sd)
			count if !missing(`type'_`i')
			local total = r(N)
			count if `type'_`i' >  3 & !missing(`type'_`i')
			di 100*r(N)/`total'
			count if `type'_`i' < -3 & !missing(`type'_`i')
			di 100*r(N)/`total'
			pause
		}
	}
}

* Re-direct measures so higher = better
* mortality
forval i = 1/7 {
	replace mortality_`i' = -mortality_`i'
}

* safety
forval i = 1/8 {
	replace safety_`i' = -safety_`i'
}

* readmission
forval i = 1/8 {
	* 9 not in 2020 data
	replace readm_`i' = -readm_`i'
}

* timely
foreach i in 1 2 5 6 7 /*8 9*/ {
	replace timely_`i' = -timely_`i'
}

* efficiency 
forval i = 1/5 {
	replace effic_`i' = -effic_`i'
}

* effectiveness 
foreach i in 4 9 11 {
	replace effec_`i' = -effec_`i'
}

* Winsorize
foreach type in mortality safety ptexp readm effic timely effec {
	forval i = 1/11 {
		cap confirm numeric variable `type'_`i' 
		if _rc {
			* skip
		}
		else {
			replace `type'_`i' = -3 if `type'_`i' < -3 & !missing(`type'_`i')
			replace `type'_`i' =  3 if `type'_`i' >  3 & !missing(`type'_`i')
		}
	}
}

/* save 																	  */
label data "Standardised using Z-scores"
compress
save data/std_method1.dta, replace


/******************************************************************************/
/* Standardise measures scores - ALTERNATE approach							  */
use data/StarRatings_cleaned.dta, clear

/* AIM
	- Get to 0-100 scale
	- Higher = better
*/

* mortality

* mortality_7 is _deaths per 1000_
replace mortality_7 = mortality_7/1000

* these are all %ages
forval i = 1/7 {
	summ mortality_`i'
	
	assert r(min) >= 0
	assert r(max) <= 1
	
	replace mortality_`i' = 1-mortality_`i'
	
	replace mortality_`i' = 100*mortality_`i'
}

summ mortality_?

* safety

* complication rate is per patient
replace safety_1 = 100*(1-safety_1)

* CLABSI and CAUTI are SIRs based on a set prediction
* know that "expected" rate for CLABSI is 25955.01 per 25969931 device-days
* https://www.cdc.gov/hai/data/portal/progress-report.html - 2018 SIR data
* 		- "2018 National and State HAI Progress Report SIR Data - Acure Care Hospitals.xls"
replace safety_2 = 100*(1-safety_2*(25955.01/25969931))

* know that "expected" rate for CAUTI is 27216.774 per 24334827 device-days
* https://www.cdc.gov/hai/data/portal/progress-report.html - 2018 SIR data
* 		- "2018 National and State HAI Progress Report SIR Data - Acure Care Hospitals.xls"
replace safety_3 = 100*(1-safety_3*(27216.774/24334827))

* SSI Colon -  8,255.389 infections per  322,125  procedures
replace safety_4 = 100*(1-safety_4*(8255.389/322125))

* SSI Hysterectomy -  1,950.352  infections per  293,503  procedures
replace safety_5 = 100*(1-safety_5*(1950.352/293503))

* MRSA - 9783.465 per  36801464 admissions
replace safety_6 = 100*(1-safety_6*(9783.465/36801464))

* C Diff - 97962.238 per 33496433 admissions
replace safety_7 = 100*(1-safety_7*(97962.238/33496433))

* PSI 90 is different

* This is:
* - 0.059841 of PSI 03 (rate per 1000)
* - 0.053497 of PSI 06 (rate per 1000)
* - 0.010097 of PSI 08 (rate per 1000)
* - 0.085335 of PSI 09 (rate per 1000)
* - 0.041015 of PSI 10 (rate per 1000)
* - 0.304936 of PSI 11 (rate per 1000)
* - 0.208953 of PSI 12 (rate per 1000)
* - 0.216046 of PSI 13 (rate per 1000)
* - 0.013269 of PSI 14 (rate per 1000)
* - 0.007011 of PSI 15 (rate per 1000)

* so PSI 90 is just a rate per 1000!

replace safety_8 = 100*(1-safety_8/1000)

* readmission

* excess days...
* 0 = best performance (no incentive <0)
* penalty grows exponentially with increasing excess days
* measure is "per 100 discharges"
* 100 = "0"
forval i = 1/3 {
	replace readm_`i' = 100*cond( ///
		readm_`i' <= 0, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (readm_`i')/50 ) /// 
	)
}

* readm_4 is "per 1000 colonoscopies"
replace readm_4 = readm_4/1000

forval i = 4/8 /*9*/ {
	summ readm_`i'
	
	assert r(min) >= 0
	assert r(max) <= 1

	replace readm_`i' = 100*(1-readm_`i')
}

summ readm_?

* timely

* Timely 1 - ED - time arrival to departure
* - Maximum score: 2 hours or less
* - dropping off after that
* - minimum score if average delay > 10 hours
summ timely_1, meanonly
gen t1_plot =(_n-1)*(r(max)/`=_N-3')
gen t1 = 100*cond( ///
		t1_plot <= 120, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (t1_plot-120)/48 ) /// 
	)
gen timely_1_plot = timely_1/60
replace t1_plot = t1_plot/60

summ timely_1, det
replace timely_1 = 100*cond( ///
		timely_1 <= 120, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (timely_1-120)/48 ) /// 
	)
summ timely_1, det

summ t1_plot
twoway	(scatter timely_1 timely_1_plot, msymb(oh) mcolor(blue%10) jitter(2) ) ///
		(line t1 t1_plot, sort lc(gs0) lw(*2) ) /// 
		,	xlabel(0 2 10 `=round(`=r(max)')', grid) ylabel(,angle(h)) ///
			plotregion(margin(l=0 b=0)) /// 
			ytitle("") ///
			ylabel(0 50 100) /// 
			xtitle("ED - median time arrival to departure, hours") /// 
			legend(off) /// 
			title("A. ED - median time arrival to departure", size(medlarge) pos(11) span) /// 
			name(t1, replace)

drop timely_1_plot t1 t1_plot

* Timely 2 - ED - time admit decision to departure
* - Maximum score: 0.5 hours or less
* - dropping off after that
* - minimum score if average delay > 4 hours
summ timely_2, meanonly
gen t2_plot =(_n-1)*(r(max)/`=_N-3')
gen t2 = 100*cond( ///
		t2_plot  <= 30, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (t2_plot-30)/24 ) /// 
	)
gen timely_2_plot = timely_2/60
replace t2_plot = t2_plot/60

summ timely_2, det
replace timely_2 = 100*cond( ///
		timely_2 <= 30, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (timely_2-30)/24 ) /// 
	)
summ timely_2, det

summ t2_plot
twoway	(scatter timely_2 timely_2_plot, msymb(oh) mcolor(blue%10) jitter(2) ) ///
		(line t2 t2_plot, sort lc(gs0) lw(*2) ) /// 
		,	xlabel(0 0.5 4 `=round(`=r(max)')', grid) ylabel(,angle(h)) ///
			plotregion(margin(l=0 b=0)) /// 
			ytitle("") ///
			ylabel(0 50 100) /// 
			xtitle("ED - median time admit decision to departure, hours") /// 
			legend(off) /// 
			title("B. ED - median time admit decision to departure", size(medlarge) pos(11) span) /// 
			name(t2, replace)

drop timely_2_plot t2 t2_plot


* Timely 5 - OP time to specialist care
* - Maximum score: 14 days or less
* - dropping off after that
* - minimum score if average wait > 126 days (18 weeks)
summ timely_5, meanonly
gen t5_plot =(_n-1)*(r(max)/`=_N-3')
gen t5 = 100*cond( ///
		t5_plot  <= 14, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (t5_plot-14)/14 ) /// 
	)
gen timely_5_plot = timely_5

summ timely_5, det
replace timely_5 = 100*cond( ///
		timely_5 <= 14, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (timely_5-14)/14 ) /// 
	)
summ timely_5, det

summ t5_plot
twoway	(scatter timely_5 timely_5_plot, msymb(oh) mcolor(blue%20) jitter(2) ) ///
		(line t5 t5_plot, sort lc(gs0) lw(*2) ) /// 
		,	xlabel(0 14 126 `=round(`=r(max)')', grid) ylabel(,angle(h)) ///
			plotregion(margin(l=0 b=0)) /// 
			ytitle("") ///
			ylabel(0 50 100) /// 
			xtitle("OP - median time to specialist care, days") /// 
			title("C. OP - median time to specialist care", size(medlarge) pos(11) span) /// 
			legend(off) /// 
			name(t5, replace)

drop timely_5_plot t5 t5_plot

* Timely 6 - OP time to ECG
* - Maximum score: 7 days or less
* - dropping off after that
* - minimum score if average wait > 30 days 
summ timely_6, meanonly
gen t6_plot =(_n-1)*(r(max)/`=_N-3')
gen t6 = 100*cond( ///
		t6_plot  <= 7, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (t6_plot-7)/3 ) /// 
	)

gen timely_6_plot = timely_6

summ timely_6, det
replace timely_6 = 100*cond( ///
		timely_6 <= 7, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (timely_6-7)/3 ) /// 
	)
summ timely_6, det

summ t6_plot
twoway	(scatter timely_6 timely_6_plot, msymb(oh) mcolor(blue%10) jitter(2) ) ///
		(line t6 t6_plot, sort lc(gs0) lw(*2) ) /// 
		,	xlabel(0 7 30 `=round(`=r(max)')', grid) ylabel(,angle(h)) ///
			plotregion(margin(l=0 b=0)) /// 
			ytitle("") ///
			ylabel(0 50 100) /// 
			xtitle("OP - median time to ECG, days") /// 
			title("D. OP - median time to ECG", size(medlarge) pos(11) span) /// 
			legend(off) ///
			name(t6, replace)

*scatter timely_6 timely_6_plot, xlabel(7 30, grid) ylabel(,angle(h))
drop timely_6_plot t6 t6_plot

* Timely 7 - ED time arrival to discharge
* - Maximum score: 1 hours or less
* - dropping off after that
* - minimum score if average delay > 6 hours
summ timely_7, meanonly
gen t7_plot =(_n-1)*(r(max)/`=_N-3')
gen t7 = 100*cond( ///
		t7_plot  <= 60, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (t7_plot-60)/40 ) /// 
	)

gen timely_7_plot = timely_7

summ timely_7, det
replace timely_7 = 100*cond( ///
		timely_7 <= 60, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (timely_7-60)/40 ) /// 
	)
summ timely_7, det

replace timely_7_plot = timely_7_plot/60
replace t7_plot = t7_plot/60
summ timely_7_plot

summ t7_plot
twoway	(scatter timely_7 timely_7_plot, msymb(oh) mcolor(blue%10) jitter(2) ) ///
		(line t7 t7_plot, sort lc(gs0) lw(*2) ) /// 
		,	xlabel(0 1 6 `=round(`=r(max)')', grid) ylabel(,angle(h)) ///
			plotregion(margin(l=0 b=0)) /// 
			ytitle("") ///
			ylabel(0 50 100) /// 
			xtitle("ED - median time to arrival to discharge, hours") /// 
			title("E. ED - median time to arrival to discharge", size(medlarge) pos(11) span) /// 
			legend(off) /// 
			name(t7, replace)

*scatter timely_7 timely_7_plot, xlabel(60 360, grid) ylabel(,angle(h))
drop timely_7_plot t7 t7_plot

graph combine t1 t2 t5 t6 t7, cols(2) l1title("Standardised score", size(small) ) ysize(8) xsize(6) altshrink
graph export results/Figure2_StarRatings_standardisation_examples.png, width(1000) replace

/*
* Timely 8 - ED time arrival to til seen
* - Maximum score: 10 minutes or less
* - dropping off after that
* - minimum score if average delay > 30 minutes
gen timely_8_plot = timely_8

replace timely_8 = 100*cond( ///
		timely_8 <= 10, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (timely_8-10)/3 ) /// 
	)
summ timely_8

scatter timely_8 timely_8_plot, xlabel(10 30, grid) ylabel(,angle(h))
drop timely_8_plot


* Timely 9 - ED time to pain treatment
* - Maximum score: 30 minutes or less
* - dropping off after that
* - minimum score if average delay > 90 minutes
gen timely_9_plot = timely_9

replace timely_9 = 100*cond( ///
		timely_9 <= 30, /// 
		1, /// 
		1+invlogit(-5)-invlogit(-5 + (timely_9-30)/7 ) /// 
	)
summ timely_9

scatter timely_9 timely_9_plot, xlabel(30 90, grid) ylabel(,angle(h))
drop timely_9_plot
*/

* efficiency 

* these are all proportions where higher = worse
* some may be appropriate? for now, just assume always inappropriate

forval i = 1/5 {
	replace effic_`i' = 100*(1-effic_`i')
}

* effectiveness 
forval i = 1/11 {
	if `i' == 3 {
		* 3 not in 2020 data
	}
	else {
		replace effec_`i' = 100*effec_`i'
	}
}
foreach i in 4 9 11 {
	replace effec_`i' = 100-effec_`i'
}


* checks...
summ mortality_?
summ safety_?
summ readm_?
summ ptexp_? ptexp_??
summ timely_?
summ effec_? effec_??
summ effic_?

label data "Standardised using reference poinst"
compress
save data/std_method2.dta, replace
