/******************************************************************************/
/* 	Measurement model impact
	
	This compares domain scores from latent variable models and domain scores
	calculated as the arithmetic mean of the known measures in each domain.
	
	Basically, this is to check that using the mean of measures in the timely 
	domain will not be too misleading.
	
	Results show that for some domains, the two are very similar, but for
	others they vary substantially.
	
	Version 1.0
	2020-11-25
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"


/******************************************************************************/
/* Measurement model for groups in Z-scored data 							  */
use data/std_method1.dta, clear

keep 	provider_id /// 
		mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??


egen mortality_nonmiss = rownonmiss(mortality_?)
sem (mortality_? <- Mortality) if mortality_nonmiss >= 3, method(mlmv)
estimates store mortality
predict mortality_sem if mortality_nonmiss >= 3, latent(Mortality)
egen 	mortality_mean = rowmean(mortality_?) if mortality_nonmiss >= 3

egen safety_nonmiss = rownonmiss(safety_?)
sem (safety_? <- Safety) if safety_nonmiss >= 3, method(mlmv)
estimates store safety
predict safety_sem if safety_nonmiss >= 3, latent(Safety)
egen 	safety_mean = rowmean(safety_?) if safety_nonmiss >= 3

egen readm_nonmiss = rownonmiss(readm_?)
sem (readm_? <- Readm) if readm_nonmiss >= 3, method(mlmv)
estimates store readm
predict readm_sem if readm_nonmiss >= 3, latent(Readm)
egen 	readm_mean = rowmean(readm_?) if readm_nonmiss >= 3

egen ptexp_nonmiss = rownonmiss(ptexp_? ptexp_??)
sem (ptexp_? ptexp_?? <- Ptexp) if ptexp_nonmiss >= 3, method(mlmv)
estimates store ptexp
predict ptexp_sem if ptexp_nonmiss >= 3, latent(Ptexp)
egen 	ptexp_mean = rowmean(ptexp_? ptexp_??) if ptexp_nonmiss >= 3

egen effic_nonmiss = rownonmiss(effic_?)
sem (effic_? <- Effic) if effic_nonmiss >= 3, method(mlmv)
estimates store effic
predict effic_sem if effic_nonmiss >= 3, latent(Effic)
egen 	effic_mean = rowmean(effic_?) if effic_nonmiss >= 3


**** DOES NOT CONVERGE
egen timely_nonmiss = rownonmiss(timely_?)
*sem (timely_? <- Timely) if timely_nonmiss >= 5, method(mlmv) difficult
*estimates store timely

gen timely_sem = .
egen timely_mean = rowmean(timely_?) if timely_nonmiss >= 3

egen effec_nonmiss = rownonmiss(effec_? effec_??)
sem (effec_? effec_?? <- Effec) if effec_nonmiss >= 3, method(mlmv)
estimates store effec
predict effec_sem if effec_nonmiss >= 3, latent(Effec)
egen 	effec_mean = rowmean(effec_? effec_??) if effec_nonmiss >= 3

keep provider_id *_sem *_mean

foreach var in mortality safety readm ptexp effic effec {

	if "`var'" == "mortality" {
		local title "A. Mortality"
	}
	if "`var'" == "safety" {
		local title "B. Safety"
	}
	if "`var'" == "readm" {
		local title "C. Readmission"
	}
	if "`var'" == "ptexp" {
		local title "D. Patient experience"
	}
	if "`var'" == "effic" {
		local title "E. Efficient use of medical imaging"
	}
	if "`var'" == "effec" {
		local title "F. Effectiveness of care"
	}

	local ytitle "LVM domain score"
	local xtitle "Mean of individual measures"
	
	pwcorr `var'_sem `var'_mean
	#delimit ;
	twoway 	(scatter `var'_sem `var'_mean, msymb(oh) mcolor(blue%30) )
			(function y=x, range(-3 3))
			,	ytitle("`ytitle'") xtitle("`xtitle'")
				legend(off)
				title("`title'", span pos(11) size(medsmall) )
				xlabel(-3(1)3) ylabel(-3(1)3, angle(h))
				name(`var', replace)
			;
	#delimit cr
}

twoway	(scatter mortality_sem mortality_mean if 1==2, msymb(oh) mcol(blue) ) ///
		(line	 mortality_sem mortality_mean if 1==2) ///
		,	plotregion(lstyle(none) margin(0 0 0 0)) /// 
			graphregion(lstyle(none) margin(0 0 0 0)) ///
			ysc(noline) xsc(noline) ylab() xlab() ///
			ytitle("") xtitle("") ///
			legend( ///
				order( ///
					1 "Individual hospital" ///
					2 "Line of equality" ///
				) ///
				cols(2) region(lstyle(none)) ///
				ring(0) pos(0) ///
			) ///
			fysize(10) /// 
			name(lvm_legend, replace)

graph combine mortality safety readm ptexp effic effec, cols(3) name(HospitalCompareGroups, replace)
graph combine HospitalCompareGroups lvm_legend, cols(1) name(HospitalCompareGroups, replace)
graph export results/exploratory/lvm_vs_mean_of_measures.png, replace width(1000)
