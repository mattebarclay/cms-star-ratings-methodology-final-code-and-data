/******************************************************************************/
/* 	LOAD DATA CMS
	
	This file loads data from a Stata file produced using STAT-transfer,
	based on a SAS bdat file of the same name, and applies initial formatting
	to the various measures.
	
	Measures are commented out when they are not in the 2020 dataset
	but were in an earlier dataset used in initial design of this study.

	Matthew Barclay
	Version 1.0
	2020-11-25
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"


/******************************************************************************/
/* Load data 																  */
*use data/StarRatings.dta, clear
use data/all_data_2020jan.dta, clear
desc

/******************************************************************************/
/* What are the (60) measures in the file?									  */
# delimit ;
local measure_all 	COMP_HIP_KNEE 
					ED_1B 
					ED_2B 
					EDAC_30_AMI 
					EDAC_30_HF 
					EDAC_30_PN 
					HAI_1 
					HAI_2 
					HAI_3 
					HAI_4 
					HAI_5 
					HAI_6 
					H_CLEAN_HSP_LINEAR 
					H_COMP_1_LINEAR 
					H_COMP_2_LINEAR 
					H_COMP_3_LINEAR 
					H_COMP_5_LINEAR 
					H_COMP_6_LINEAR 
					H_COMP_7_LINEAR 
					H_HSP_RATING_LINEAR 
					H_QUIET_HSP_LINEAR 
					H_RECMND_LINEAR 
					IMM_2 
					IMM_3_OP_27 
					MORT_30_AMI 
					MORT_30_CABG 
					MORT_30_COPD 
					MORT_30_HF 
					MORT_30_PN 
					MORT_30_STK 
					OP_1 
					OP_2 
					OP_3B 
					OP_4 
					OP_5 
					OP_8 
					OP_10 
					OP_11 
					OP_13 
					OP_14 
					OP_18B 
					OP_20 
					OP_21 
					OP_22 
					OP_23 
					OP_29 
					OP_30 
					OP_32 
					OP_33 
					PC_01 
					PSI_4_SURG_COMP 
					PSI_90_SAFETY 
					READM_30_CABG 
					READM_30_COPD 
					READM_30_HIP_KNEE 
					READM_30_HOSP_WIDE  
					READM_30_STK 
					SEP_1 
					VTE_6
					;

* denominator for patient experience measures

*PtExp_DEN= 
gen H_CLEAN_HSP_LINEAR_DEN = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_COMP_1_LINEAR_DEN    = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_COMP_2_LINEAR_DEN    = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_COMP_3_LINEAR_DEN    = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_COMP_5_LINEAR_DEN    = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_COMP_6_LINEAR_DEN    = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_COMP_7_LINEAR_DEN    = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_HSP_RATING_LINEAR_DEN= H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_CLEAN_HSP_LINEAR_DEN = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_QUIET_HSP_LINEAR_DEN = H_NUMB_COMP*H_RESP_RATE_P/100;
gen H_RECMND_LINEAR_DEN    = H_NUMB_COMP*H_RESP_RATE_P/100;					

# delimit cr


/******************************************************************************/
/* Apply exclusions	to measures												  */

* combine IMM_3 and OP_27 into IMM_3_OP_27
* op_27 not reported in 2020
gen IMM_3_OP_27 = .
replace IMM_3_OP_27 = IMM_3 if !missing(IMM_3)
*replace IMM_3_OP_27 = OP_27 if  missing(IMM_3) & !missing(OP_27)

gen IMM_3_OP_27_DEN = .
replace IMM_3_OP_27_DEN = IMM_3_DEN if !missing(IMM_3)
*replace IMM_3_OP_27_DEN = OP_27_DEN if  missing(IMM_3) & !missing(OP_27)

* As IMM_3 and OP_27 are combined in IMM_3_OP_27, they can be discarded
drop IMM_3
drop IMM_3_DEN
*drop OP_27
*drop OP_27_DEN

* Denominator for HAI_* is the Volume measure
rename (HAI_*_DEN_VOL) (HAI_*_DEN)


* Set measures to missing if they do not have a denominator
* or if denominator is 0
qui foreach var of varlist *_DEN {
	local var2 = substr("`var'",1,length("`var'")-4)
	di as text _newline "`var2', `var'"
	replace `var2' = . if `var' == .
	replace `var2' = . if `var' == 0
}

/* Measures where fewer than 100 total hospitals report the measure will be 
	excluded. */
qui foreach var of varlist *_DEN {
	local var2 = substr("`var'",1,length("`var'")-4)
	
	gen `var2'_nm = !missing(`var2')
	egen `var2'_nmt = total(`var2'_nm)
	drop `var2'_nm
	
	local nmt = `var2'_nmt[1]
	nois di as text _newline "`var2' non-missing for `nmt' hospitals. " _cont 
	
	if `nmt' < 100 {
		nois di as error "Dropping `var2'. " _cont
		
		drop `var2' 
		drop `var' 
	}
	drop `var2'_nmt
}
	
/******************************************************************************/
/* Assign measures to groups, rename, and label for sanity					  */

* Measures in mortality group
rename (MORT_30_AMI*) 		(mortality_1*)
rename (MORT_30_CABG*)		(mortality_2*)
rename (MORT_30_COPD*)	 	(mortality_3*)
rename (MORT_30_HF*) 		(mortality_4*)
rename (MORT_30_PN*) 		(mortality_5*)
rename (MORT_30_STK*) 		(mortality_6*)
rename (PSI_4_SURG_COMP*) 	(mortality_7*)
rename (mortality_*), lower

label var mortality_1 		"30-day mortality from AMI"
label var mortality_1_den 	"30-day mortality from AMI denominator"
label var mortality_2 		"30-day mortality from CABG"
label var mortality_2_den 	"30-day mortality from CABG denominator"
label var mortality_3 		"30-day mortality from COPD"
label var mortality_3_den 	"30-day mortality from COPD denominator"
label var mortality_4 		"30-day mortality from HF"
label var mortality_4_den 	"30-day mortality from HF denominator"
label var mortality_5 		"30-day mortality from PN"
label var mortality_5_den 	"30-day mortality from PN denominator"
label var mortality_6 		"30-day mortality from STK"
label var mortality_6_den 	"30-day mortality from STK denominator"
label var mortality_7 		"30-day mortality from surg comps"
label var mortality_7_den 	"30-day mortality from surg comps denominator"

* Measures in safety group
rename (COMP_HIP_KNEE*) 	(safety_1*)
rename (HAI_1*) 			(safety_2*)
rename (HAI_2*) 			(safety_3*)
rename (HAI_3*) 			(safety_4*)
rename (HAI_4*)				(safety_5*)
rename (HAI_5*) 			(safety_6*)
rename (HAI_6*) 			(safety_7*)
rename (PSI_90_SAFETY*) 	(safety_8*)
rename safety_*, lower

label var safety_1		"Hip and knee surgery complications"
label var safety_1_den 	"Hip and knee surgery complications denominator"
label var safety_2		"CLABSI"
label var safety_2_den 	"CLABSI denominator"
label var safety_3		"CAUTI"
label var safety_3_den 	"CAUTI denominator"
label var safety_4		"SSI Colon"
label var safety_4_den 	"SSI Colon denominator"
label var safety_5		"SSI Hysterectomy"
label var safety_5_den 	"SSI Hysterectomy denominator"
label var safety_6		"MRSA"
label var safety_6_den 	"MRSA denominator"
label var safety_7		"C diff"
label var safety_7_den 	"C diff denominator"
label var safety_8		"PSI-90"
label var safety_8_den 	"PSI-90 denominator"

* Measures in readmission group
rename (EDAC_30_AMI*) 			(readm_1*)
rename (EDAC_30_HF*) 			(readm_2*)
rename (EDAC_30_PN*) 			(readm_3*)
rename (OP_32*) 				(readm_4*)
rename (READM_30_CABG*) 		(readm_5*)
rename (READM_30_COPD*) 		(readm_6*)
rename (READM_30_HIP_KNEE*) 	(readm_7*)
rename (READM_30_HOSP_WIDE*) 	(readm_8*)
* stroke readmission not in new star ratings
*rename (READM_30_STK*)		 	(readm_9*)
rename readm_*, lower

label var readm_1		"Excess days in acute care AMI"
label var readm_1_den 	"Excess days in acute care AMI denominator"
label var readm_2		"Excess days in acute care HF"
label var readm_2_den 	"Excess days in acute care HF denominator"
label var readm_3		"Excess days in acute care PN"
label var readm_3_den 	"Excess days in acute care PN denominator"
label var readm_4		"Hospital visits after OP colonoscopy"
label var readm_4_den 	"Hospital visits after OP colonoscopy denominator"
label var readm_5		"Readmission following CABG"
label var readm_5_den 	"Readmission following CABG denominator"
label var readm_6		"Readmission following COPD"
label var readm_6_den 	"Readmission following COPD denominator"
label var readm_7		"Readmission following hip and knee surgery"
label var readm_7_den 	"Readmission following hip and knee surgery denominator"
label var readm_8		"Hospital-wide readmission"
label var readm_8_den 	"Hospital-wide readmission denominator"
*label var readm_9		"Readmission following STK"
*label var readm_9_den 	"Readmission following STK denominator"

* Measures in patient experience group
rename (H_COMP_1_LINEAR*) (ptexp_1*)
rename (H_COMP_2_LINEAR*) (ptexp_2*)
rename (H_COMP_3_LINEAR*) (ptexp_3*)
rename (H_COMP_5_LINEAR*) (ptexp_4*)
rename (H_COMP_6_LINEAR*) (ptexp_5*)
rename (H_COMP_7_LINEAR*) (ptexp_6*)
rename (H_CLEAN_HSP_LINEAR*) (ptexp_7*)
rename (H_QUIET_HSP_LINEAR*) (ptexp_8*)
rename (H_HSP_RATING_LINEAR*) (ptexp_9*)
rename (H_RECMND_LINEAR*) (ptexp_10*)
rename ptexp_*, lower

label var ptexp_1 		"Communication with nurses"
label var ptexp_1_den	"Communication with nurses denominator"
label var ptexp_2 		"Communication with doctors"
label var ptexp_2_den	"Communication with doctors denominator"
label var ptexp_3 		"Responsiveness of hospital staff"
label var ptexp_3_den	"Responsiveness of hospital staff denominator"
label var ptexp_4 		"Communication about medicines"
label var ptexp_4_den	"Communication about medicines denominator"
label var ptexp_5 		"Discharge information"
label var ptexp_5_den	"Discharge information denominator"
label var ptexp_6 		"Care transition"
label var ptexp_6_den	"Care transition denominator"
label var ptexp_7 		"Cleanliness of hosp env"
label var ptexp_7_den	"Cleanliness of hosp env denominator"
label var ptexp_8 		"Quietness of hosp env"
label var ptexp_8_den	"Quietness of hosp env denominator"
label var ptexp_9 		"Global hospital rating"
label var ptexp_9_den	"Global hospital rating denominator"
label var ptexp_10 		"Willingness to recommend hospital"
label var ptexp_10_den	"Willingness to recommend hospital denominator"

* Measures in efficiency group
rename (OP_8*) 	(effic_1*)
rename (OP_10*) (effic_2*)
rename (OP_11*) (effic_3*)
rename (OP_13*) (effic_4*)
rename (OP_14*) (effic_5*)
rename effic_*, lower

label var effic_1	 	"MRI lumbar spine for low back pain"
label var effic_1_den 	"MRI lumbar spine for low back pain denominator"
label var effic_2	 	"Abdomen CT"
label var effic_2_den 	"Abdomen CT denominator"
label var effic_3	 	"Thorax CT"
label var effic_3_den 	"Thorax CT denominator"
label var effic_4	 	"Cardiac imaging for preop risk assess"
label var effic_4_den 	"Cardiac imaging for preop risk assess denominator"
label var effic_5	 	"Brain and sinus CT"
label var effic_5_den 	"Brain and sinus CT denominator"

* Measures in timeliness group
rename (ED_1B*) 	(timely_1*)
rename (ED_2B*) 	(timely_2*)
*rename (OP_1 OP_1_DEN) (timely_3 timely_3_den)
*rename (OP_2 OP_2_DEN)	(timely_4 timely_4_den)
rename (OP_3B*) 	(timely_5*)
rename (OP_5*) 		(timely_6*)
rename (OP_18B*) 	(timely_7*)
* OP_20, OP_21 not in new star ratings
*rename (OP_20*) 	(timely_8*)
*rename (OP_21*) 	(timely_9*)
rename timely_*, lower

label var timely_1	 	"ED - time arrival to departure"
label var timely_1_den 	"ED - time arrival to departure denominator"
label var timely_2	 	"ED - time admit decision to departure"
label var timely_2_den 	"ED - time admit decision to departure denominator"
*label var timely_3	 	"OP - time to blood clot trt"
*label var timely_3_den 	"OP - time to blood clot trt denominator"
*label var timely_4	 	"OP - blood clot trt within 30 mins"
*label var timely_4_den 	"OP - blood clot trt within 30 mins denominator"
label var timely_5	 	"OP - time to specialist care"
label var timely_5_den 	"OP - time to specialist care denominator"
label var timely_6	 	"OP - time to ECG"
label var timely_6_den 	"OP - time to ECG denominator"
label var timely_7	 	"ED - time arrival to discharge"
label var timely_7_den 	"ED - time arrival to discharge denominator"
*label var timely_8	 	"ED - time until seen"
*label var timely_8_den 	"ED - time until seen denominator"
*label var timely_9	 	"ED - time to pain trt"
*label var timely_9_den 	"ED - time to pain trt denominator"

* Measures in effectiveness group
rename (IMM_2*)			(effec_1*)
rename (IMM_3_OP_27*)	(effec_2*)
* OP 4 not in new star ratings
*rename (OP_4*)			(effec_3*)
rename (OP_22*)			(effec_4*)
rename (OP_23*)			(effec_5*)
rename (OP_29*)			(effec_6*)
rename (OP_30*)			(effec_7*)
rename (OP_33*)			(effec_8*)
rename (PC_01*)			(effec_9*)
rename (SEP_1*)			(effec_10*)
rename (VTE_6*)			(effec_11*)
rename effec_*, lower

label var effec_1		"Flu vaccinations"
label var effec_1_den 	"Flu vaccinations denominator"
label var effec_2		"Staff flu vaccinations"
label var effec_2_den	"Staff flu vaccinations denominator"
*label var effec_3		"Chest pain getting aspirin"
*label var effec_3_den	"Chest pain getting aspirin denominator"
label var effec_4		"% patients leaving ED unseen"
label var effec_4_den	"% patients leaving ED unseen denominator"
label var effec_5		"Stroke - brain scan within 45 mins"
label var effec_5_den	"Stroke - brain scan within 45 mins denominator"
label var effec_6		"% receiving approp follow-up after colonsc"
label var effec_6_den	"% receiving approp follow-up after colonsc denominator"
label var effec_7		"% Hx polyps receiving follow-up colonsc" 
label var effec_7_den	"% Hx polyps receiving follow-up colonsc denominator"
label var effec_8		"% radiation therapy for bone mets"
label var effec_8_den	"% radiation therapy for bone mets denominator"
label var effec_9		"% mothers delivering early unnec"
label var effec_9_den	"% mothers delivering early unnec denominator"
label var effec_10		"% approp care for sepsis"
label var effec_10_den	"% approp care for sepsis denominator"
label var effec_11		"% blood clot while no prevention"
label var effec_11_den	"% blood clot while no prevention denominator"

* get rid of things we don't need?
drop H_RESP_RATE_P
drop H_NUMB_COMP
drop safety*pred

* make provider ID lower case
rename (PROVIDER_ID) (provider_id)
label var provider_id "Hospital ID"

#delimit ;

order 	provider_id
		mortality_1*
		mortality_2*
		mortality_3*
		mortality_4*
		mortality_5*
		mortality_6*
		mortality_7*
		safety_1*
		safety_2*
		safety_3*
		safety_4*
		safety_5*
		safety_6*
		safety_7*
		safety_8*
		readm_1*
		readm_2*
		readm_3*
		readm_4*
		readm_5*
		readm_6*
		readm_7*
		readm_8*
		/* readm_9* */
		ptexp_1*
		ptexp_2*
		ptexp_3*
		ptexp_4*
		ptexp_5*
		ptexp_6*
		ptexp_7*
		ptexp_8*
		ptexp_9*
		ptexp_10*
		effic_1*
		effic_2*
		effic_3*
		effic_4*
		effic_5*	
		timely_1*
		timely_2*
		timely_5*
		timely_6*
		timely_7*
		/* timely_8* */
		/* timely_9* */
		effec_1
		effec_1_den
		effec_2*
		/* effec_3* */
		effec_4*
		effec_5*
		effec_6*
		effec_7*
		effec_8*
		effec_9*
		effec_10*
		effec_11*
		;
	
#delimit cr


/******************************************************************************/
/* Remove hospitals without any measures									  */
egen nm = rownonmiss(mortality* safety* readm* effic* timely* effec*)
drop if nm == 0
drop nm


/******************************************************************************/
/* Save																		  */
label data "CMS Star Ratings data for January 2020, cleaned for analysis."
compress
save data/StarRatings_cleaned.dta, replace
