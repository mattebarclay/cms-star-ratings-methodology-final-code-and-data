/******************************************************************************/
/* 	Impact of missing domains
	
	This looks at the amount of missing data, and the ranks of hospitals.
	
	Various bits of information that was copied out into tables for the thesis.
	
	Version 1.0
	2020-11-25
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"

/* Ranks by amount of missing infomrmation */
use data/approaches_combined.dta, clear

table grp1_cat, c(mean std1_grp1_wt1_rank min std1_grp1_wt1_rank max std1_grp1_wt1_rank)
table grp1_cat, c(mean std2_grp1_wt1_rank min std2_grp1_wt1_rank max std2_grp1_wt1_rank)
table grp1_cat, c(mean std1_grp2_wt1_rank min std1_grp2_wt1_rank max std1_grp2_wt1_rank)
table grp1_cat, c(mean std1_grp1_wt2_rank min std1_grp1_wt2_rank max std1_grp1_wt2_rank)

/* Amount of missing information by domain */
use data/std_method1_grp_pol.dta, clear

egen number_missing = rowmiss(*_m1)
egen non_missing = rownonmiss(*_m1)
keep if non_missing >= 3 & (!missing(mortality_m1) | !missing(safety_m1) | !missing(readm_m1))

foreach var in mortality safety readm ptexp effic timely effec {
	gen `var'_miss = missing(`var'_m1)
}

replace number_missing = 3 if number_missing >= 3

expand 2, gen(exp)
replace number_missing = -2 if exp

drop exp
expand 2 if number_missing >= 1, gen(exp)

replace number_missing = -1 if number_missing == 0
replace number_missing = 0 if exp

replace number_missing = 3 if number_missing >= 3

label define number_missing -2 "All hospitals" -1 "No missing" 0 "Any missing" 3 "3-4 missing", replace
label values number_missing number_missing

gen n = 1
collapse (sum) n (mean) *_miss, by(number_missing)
list

/* Scores on absolute standardisation on each domain */
use data/std_method2_grp_pol.dta, clear


egen number_missing = rowmiss(*_m2)
egen non_missing = rownonmiss(*_m2)
keep if non_missing >= 3 & (!missing(mortality_m2) | !missing(safety_m2) | !missing(readm_m2))

foreach var in mortality safety readm ptexp effic timely effec {
	gen `var'_miss = missing(`var'_m2)
}

replace number_missing = 3 if number_missing >= 3

expand 2, gen(exp)
replace number_missing = -2 if exp

drop exp
expand 2 if number_missing >= 1, gen(exp)

replace number_missing = -1 if number_missing == 0
replace number_missing = 0 if exp

replace number_missing = 3 if number_missing >= 3

label define number_missing -2 "All hospitals" -1 "No missing" 0 "Any missing" 3 "3-4 missing", replace
label values number_missing number_missing

gen n = 1
collapse (sum) n (mean) *_m2, by(number_missing)
list

collapse (mean) *_m2
list

use data/std_method2_grp_pol.dta, clear

collapse (mean) *_m2
list

