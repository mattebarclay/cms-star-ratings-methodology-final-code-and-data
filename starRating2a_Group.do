/******************************************************************************/
/* 	Group
	
	Original groups are well-defined. This uses factor analysis (ML) to identify 
	groups from the Z-scored and reference-grouped data.
	
	2020-11-25
	Version 1.0
*/

clear
cap log c


/******************************************************************************/
/* Change directory															  */
cd "U:\My Documents\PhD\cms-star-ratings-methodology-final-code-and-data"

log using StarRatings_efa_log.smcl, replace

/******************************************************************************/
/* Find groups in Z-scored data 											  */
use data/StarRatingsScenarios/std_method1.dta, clear

keep 	provider_id /// 
		mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??

* perform factor analysis with incomplete data
* see:
*	https://stats.idre.ucla.edu/stata/faq/how-can-i-do-factor-analysis-with-missing-data-in-stata/
mi set mlong

* ignore high missing variables
*mi misstable summarize
*rename (safety_5 timely_5 effec_8) ignore_=

* can now get a "cc" correlation matrix (based on 80 observations!)
/*corr	mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??* ///
		,	cov
*/

* register as imputed
mi register imputed mortality_? /// 
					safety_? ///
					readm_? ///
					ptexp_? ptexp_?? /// 
					effic_? ///
					timely_? ///
					effec_? effec_??*

* calculate ML covariance matrix using expectation-maximisation
mi impute mvn 	mortality_? /// 
				safety_? ///
				readm_? ///
				ptexp_? ptexp_?? /// 
				effic_? ///
				timely_? ///
				effec_? effec_??* /// 
				,	emonly(iter(100000)) 
				
matrix cov_em = r(Sigma_em)
matrix list cov_em

* how many?
factormat cov_em, n(`=_N') ml
screeplot

* argues for 6 or 7 in the elbow?
* argues for 7 based on eigenvalues > 1
* 7 factors explain >0.8 of the variance

factormat cov_em, n(`=_N') ml factors(6)
rotate, promax normalize 

* see excel file for details of factor assignments

/******************************************************************************/
/* Find groups in reference-standardised data								  */
use data/std_method2.dta, clear

keep 	provider_id /// 
		mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??

* perform factor analysis with incomplete data
* see:
*	https://stats.idre.ucla.edu/stata/faq/how-can-i-do-factor-analysis-with-missing-data-in-stata/
mi set mlong

* ignore high missing variables
*mi misstable summarize
*rename (safety_5 timely_5 effec_8) ignore_=

* can now get a "cc" correlation matrix (based on 80 observations!)
/*corr	mortality_? /// 
		safety_? ///
		readm_? ///
		ptexp_? ptexp_?? /// 
		effic_? ///
		timely_? ///
		effec_? effec_??* ///
		,	cov
*/
* register as imputed
mi register imputed mortality_? /// 
					safety_? ///
					readm_? ///
					ptexp_? ptexp_?? /// 
					effic_? ///
					timely_? ///
					effec_? effec_??*

* calculate ML covariance matrix using expectation-maximisation
mi impute mvn 	mortality_? /// 
				safety_? ///
				readm_? ///
				ptexp_? ptexp_?? /// 
				effic_? ///
				timely_? ///
				effec_? effec_??* /// 
				,	emonly(iter(100000)) 
				
matrix cov_em = r(Sigma_em)
matrix list cov_em

* how many?
factormat cov_em, n(`=_N') ml
screeplot

* argues for 6 or 7 in the elbow?
* argues for 6 based on eigenvalues > 1
* 6 factors explain >0.8 of the variance and matches other approach

factormat cov_em, n(`=_N') ml factors(6)
rotate, promax normalize 

* see excel file for details of factor assignments

log close
